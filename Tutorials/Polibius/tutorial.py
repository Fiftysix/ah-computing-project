"""
This file will be executed with globals:
    "TutorialBase": TutorialBase
    "Handler": <Tutorials.handler.TutorialHandler object at 0x0...>
    "__name__": "Tutorial"
"""
from PyQt5 import QtCore
from PyQt5.QtCore import Qt
import extra
import string


class Tutorial(TutorialBase):
    def __init__(self, message, path):
        TutorialBase.__init__(self, path)
        self.num_pages = 5
        self.grid = None
        self.message = None
        self.encrypted = None
        self.decrypting = False

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.send_letter)
        self.encrypted_message = ""
        self.flying_timer_idx = 0
        self.flying_letters = []

        self.setup()

    def graphic_1(self, graphic_level):
        self.clear_graphic()
        image = self.media.image_widget("cipher.png")
        self.graphics_widget.layout().addWidget(image)

    def graphic_2(self, graphic_level):
        self.clear_graphic()
        self.remove_flying_letters()
        self.message = extra.ValueLabel("Message", "SECRET MESSAGE")
        self.graphics_widget.layout().addWidget(self.message, 0, 0)

        alpha_grid = [[""]+list("12345"), ["1"]]
        column = 0
        row = 1
        for letter in string.ascii_uppercase:
            if column == 5:
                column = 0
                row += 1
                alpha_grid += [[str(row)]]
            if letter == "J":  # combine I and J
                alpha_grid[-1][-1] += "/"+letter
                column -= 1
            else:
                alpha_grid[-1] += [letter]
            column += 1

        self.grid = extra.LetterGrid(alpha_grid, header=True)
        self.graphics_widget.layout().addWidget(self.grid, 2, 0)
        self.grid.animate_display()

        self.encrypted = extra.ValueLabel("Encrypted Message", "", hidden=True)
        self.graphics_widget.layout().addWidget(self.encrypted, 3, 0)

    def graphic_3(self, graphic_level):
        if graphic_level != 2:
            self.graphic_2(graphic_level)
        self.encrypted.add_value("")
        self.grid.end_animation()

        self.encrypted_message = ""

        self.decrypting = False
        self.flying_timer_idx = 0
        self.send_letter()

        self.timer.setInterval(6000)
        self.timer.start()

    def send_letter(self):
        letter = "SECRET MESSAGE"[self.flying_timer_idx]
        msg = self.message.get_position(self.flying_timer_idx, "SECRET MESSAGE")

        if letter not in string.ascii_uppercase:
            end = self.encrypted.get_next_position(self.encrypted_message, letter)
            self.encrypted_message += letter
            path_a = [msg, end]
            path_b = [msg, end]
        else:
            index = string.ascii_uppercase.find(letter)
            if index > 8:
                index -= 1
            x = index % 5 + 1
            y = index // 5 + 1

            mid = self.grid.convert_positions([[x, y], [x, 0], [0, y]])
            encrypted_letter = mid[1]["value"] + mid[2]["value"]

            end_a = self.encrypted.get_next_position(self.encrypted_message, encrypted_letter[0])
            end_b = self.encrypted.get_next_position(self.encrypted_message+encrypted_letter[0], encrypted_letter[1])
            self.encrypted_message += encrypted_letter
            path_a = [msg, mid[0], mid[1], end_a]
            path_b = [msg, mid[0], mid[2], end_b]
            if self.decrypting:
                path_a = path_a[::-1]
                path_b = path_b[::-1]
        if self.decrypting:
            on_end_a = lambda l: self.message.add_value(l)
            on_end_b = lambda l: 0
        else:
            on_end_a = lambda l: self.encrypted.add_value(l)
            on_end_b = lambda l: self.encrypted.add_value(l)
        self.flying_letters += [extra.FlyingLetter(path_a, self.graphics_widget, on_end_a)]
        self.flying_letters += [extra.FlyingLetter(path_b, self.graphics_widget, on_end_b)]

        self.flying_timer_idx += 1
        if self.flying_timer_idx >= len("SECRET MESSAGE"):
            self.timer.stop()
        self.timer.setInterval(1600)  # letters after the first one have a shorter delay

    def remove_flying_letters(self):
        if len(self.flying_letters) > 0:
            for letter in self.flying_letters:
                letter.stop_animation()
            for child in self.graphics_widget.children():
                if type(child) == extra.FlyingLetter:
                    child.setParent(None)
                    child.deleteLater()

            self.timer.stop()
            self.flying_letters = []

            self.encrypted.value = "3451311245144 23513434112251"
            self.encrypted.add_value("")

    def graphic_4(self, graphic_level):
        if graphic_level != 3:
            self.graphic_3(graphic_level)
        self.remove_flying_letters()

        self.message.value = ""
        self.message.add_value("")

        self.decrypting = True
        self.encrypted_message = ""
        self.flying_timer_idx = 0

        self.send_letter()

        self.timer.setInterval(6000)
        self.timer.start()

    def graphic_5(self, graphic_level):
        if graphic_level != 4:
            self.graphic_4(graphic_level)
        self.remove_flying_letters()
        self.message.value = "SECRET MESSAGE"
        self.message.add_value("")
