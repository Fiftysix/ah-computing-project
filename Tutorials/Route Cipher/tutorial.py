"""
This file will be executed with globals:
    "TutorialBase": TutorialBase
    "Handler": <Tutorials.handler.TutorialHandler object at 0x0...>
    "__name__": "Tutorial"
"""
from PyQt5 import QtCore
from PyQt5.QtCore import Qt
import extra
import string


class Tutorial(TutorialBase):
    def __init__(self, message, path):
        TutorialBase.__init__(self, path)
        self.num_pages = 5
        self.grid = None
        self.message = None
        self.encrypted = None
        self.decrypting = False
        self.message_text = message

        # TODO: don't use hardcoded path
        self.line_path = [(0,0),(0,1),(0,2),(1,2),(1,1),(1,0),(2,0),(2,1),(2,2),(3,2),(3,1),(3,0),(4,0),(4,1),(4,2),(4,3),(3,3),(2,3),(1,3),(0,3)]
        grid_width = 5
        self.alpha_grid = [list(self.message_text[x:x+grid_width]) for x in range(0, len(self.message_text), grid_width)]

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.send_letter)
        self.encrypted_message = ""
        self.flying_timer_idx = 0
        self.flying_letters = []

        self.setup()

    def graphic_1(self, graphic_level):
        self.clear_graphic()
        image = self.media.image_widget("cipher.png")
        self.graphics_widget.layout().addWidget(image)

    def graphic_2(self, graphic_level):
        self.clear_graphic()
        self.remove_flying_letters()
        self.message = extra.ValueLabel("Message", self.message_text)
        self.graphics_widget.layout().addWidget(self.message, 0, 0)

        self.grid = extra.LetterGrid(self.alpha_grid)
        self.graphics_widget.layout().addWidget(self.grid, 2, 0)
        self.grid.animate_display(on_end=lambda: self.grid.draw_path(self.line_path))

        self.encrypted = extra.ValueLabel("Encrypted Message", "", hidden=True)
        self.graphics_widget.layout().addWidget(self.encrypted, 3, 0)

    def graphic_3(self, graphic_level):
        if graphic_level != 2:
            self.graphic_2(graphic_level)
        self.encrypted.add_value("")
        self.grid.end_animation()

        self.encrypted_message = ""

        self.decrypting = False
        self.flying_timer_idx = 0
        self.send_letter()

        self.timer.setInterval(1000)
        self.timer.start()

    def send_letter(self):
        if self.flying_timer_idx >= len(self.message_text):
            self.timer.stop()

            if self.decrypting:
                for y in range(4):
                    for x in range(5):
                        start = self.grid.convert_positions([(x, y)])[0]
                        letter = self.alpha_grid[y][x]
                        end = self.message.get_next_position(self.message_text[:y*5+x], letter)
                        self.flying_letters += [extra.FlyingLetter([start, end], self.graphics_widget, lambda l: self.message.add_value(l), duration=3000)]
            return

        pos = self.line_path[self.flying_timer_idx]
        start = self.grid.convert_positions([pos])[0]
        letter = self.alpha_grid[pos[1]][pos[0]]
        start["value"] = letter
        end = self.encrypted.get_next_position(self.encrypted_message, letter)
        self.encrypted_message += letter

        if self.decrypting:
            path = [end, start]

            def on_end(l):
                self.grid.rows[pos[1]][pos[0]] = l
                self.grid.repaint()
        else:
            path = [start, end]
            on_end = lambda l: self.encrypted.add_value(l)
        self.flying_letters += [extra.FlyingLetter(path, self.graphics_widget, on_end, duration=2000)]

        self.flying_timer_idx += 1
        if self.flying_timer_idx >= len(self.message_text):
            self.timer.setInterval(3000)

    def remove_flying_letters(self):
        if len(self.flying_letters) > 0:
            for letter in self.flying_letters:
                letter.stop_animation()
            for child in self.graphics_widget.children():
                if type(child) == extra.FlyingLetter:
                    child.setParent(None)
                    child.deleteLater()

            self.timer.stop()
            self.flying_letters = []

            self.encrypted.value = "S ETSUPE MCERREEGASS"
            self.encrypted.add_value("")

    def graphic_4(self, graphic_level):
        if graphic_level != 3:
            self.graphic_3(graphic_level)
        self.remove_flying_letters()

        self.grid.rows = [[""]*5 for x in range(4)]
        self.grid.repaint()

        self.message.value = ""
        self.message.add_value("")

        self.decrypting = True
        self.encrypted_message = ""
        self.flying_timer_idx = 0

        self.send_letter()
        self.timer.setInterval(1000)
        self.timer.start()

    def graphic_5(self, graphic_level):
        if graphic_level != 4:
            self.graphic_4(graphic_level)
        self.remove_flying_letters()
        self.grid.rows = self.alpha_grid
        self.grid.repaint()
        self.message.value = self.message_text
        self.message.add_value("")
