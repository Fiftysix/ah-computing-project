Summary
Write out the pigpen alphabet (using # and x shapes)
Fill the shapes with the alphabet.
Replace each letter in the message with the shape surrounding that letter in the pigpen alphabet.