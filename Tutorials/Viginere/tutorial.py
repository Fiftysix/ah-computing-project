"""
This file will be executed with globals:
    "TutorialBase": TutorialBase
    "Handler": <Tutorials.handler.TutorialHandler object at 0x0...>
    "__name__": "Tutorial"
"""
from PyQt5 import QtCore
import extra
import string


class Tutorial(TutorialBase):
    def __init__(self, message, path):
        TutorialBase.__init__(self, path)
        self.num_pages = 6
        self.grid = None
        self.message = None
        self.encrypted = None
        self.key = None
        self.decrypting = False

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.send_letter)
        self.encrypted_message = ""
        self.flying_timer_idx = 0
        self.flying_letters = []

        self.setup()

    def graphic_1(self, graphic_level):
        self.clear_graphic()
        image = self.media.image_widget("cipher.png")
        self.graphics_widget.layout().addWidget(image)

    def graphic_2(self, graphic_level):
        self.clear_graphic()
        self.remove_flying_letters()
        self.message = extra.ValueLabel("Message", "SECRET MESSAGE")
        self.graphics_widget.layout().addWidget(self.message, 0, 0)

        self.key = extra.ValueLabel("Keyword", "KEYWOR DKEYWOR")
        self.graphics_widget.layout().addWidget(self.key, 1, 0)

        alpha_grid = [[""]+list(string.ascii_uppercase)]
        alpha = string.ascii_uppercase
        for letter in string.ascii_uppercase:
            alpha_grid += [[letter]+list(alpha)]
            alpha = alpha[1:]+alpha[0]

        self.grid = extra.LetterGrid(alpha_grid, hidden=True, header=True)
        self.graphics_widget.layout().addWidget(self.grid, 2, 0)

        self.encrypted = extra.ValueLabel("Encrypted Message", "", hidden=True)
        self.graphics_widget.layout().addWidget(self.encrypted, 3, 0)

    def graphic_3(self, graphic_level):
        if graphic_level != 2:
            self.graphic_2(graphic_level)
        self.grid.animate_display()

    def graphic_4(self, graphic_level):
        if graphic_level != 3:
            self.graphic_3(graphic_level)
        self.encrypted.add_value("")
        self.grid.end_animation()

        self.encrypted_message = ""

        self.decrypting = False
        self.flying_timer_idx = 0
        self.send_letter()

        self.timer.setInterval(6000)
        self.timer.start()

    def send_letter(self):
        msg_letter = self.message.value[self.flying_timer_idx]
        msg = self.message.get_position(self.flying_timer_idx)
        key = self.key.get_position(self.flying_timer_idx)

        if msg_letter not in string.ascii_uppercase:
            end = self.encrypted.get_next_position(self.encrypted_message, msg_letter)
            self.encrypted_message += msg_letter
            msg_path = [msg, end]
            key_path = [key, end]
        else:
            x = string.ascii_uppercase.find(msg_letter) + 1
            y = string.ascii_uppercase.find(self.key.value[self.flying_timer_idx]) + 1

            if self.decrypting:
                mid = self.grid.convert_positions([[0, y]])
                mid += self.grid.get_position(msg_letter, y, [0])
            else:
                mid = self.grid.convert_positions([[x, 0], [0, y], [x, y]])
            encrypted_letter = mid[2]["value"]

            end = self.encrypted.get_next_position(self.encrypted_message, encrypted_letter)
            self.encrypted_message += encrypted_letter
            if self.decrypting:
                msg_path = [msg, msg, mid[1], mid[2], end]
                key_path = [key, mid[0], mid[1]]
            else:
                msg_path = [msg, mid[0], mid[2], end]
                key_path = [key, mid[1], mid[2]]
        self.flying_letters += [extra.FlyingLetter(msg_path, self.graphics_widget, lambda l: self.encrypted.add_value(l))]
        self.flying_letters += [extra.FlyingLetter(key_path, self.graphics_widget, duration=6000/(len(msg_path)-1))]

        self.flying_timer_idx += 1
        if self.flying_timer_idx >= len(self.message.value):
            self.timer.stop()
        self.timer.setInterval(2000)  # letters after the first one have a shorter delay

    def remove_flying_letters(self):
        if len(self.flying_letters) > 0:
            for letter in self.flying_letters:
                letter.stop_animation()
            for child in self.graphics_widget.children():
                if type(child) == extra.FlyingLetter:
                    child.setParent(None)
                    child.deleteLater()

            self.timer.stop()
            self.flying_letters = []

            self.encrypted.value = "CIANSK POWQWUV"
            self.encrypted.add_value("")

    def graphic_5(self, graphic_level):
        if graphic_level != 4:
            self.graphic_4(graphic_level)
        self.remove_flying_letters()

        self.message.name = "Encrypted Message"
        self.message.value = "CIANSK POWQWUV"
        self.message.add_value("")
        self.encrypted.name = "Message"
        self.encrypted.value = ""
        self.encrypted.add_value("")

        self.decrypting = True
        self.encrypted_message = ""
        self.flying_timer_idx = 0

        self.send_letter()

        self.timer.setInterval(6000)
        self.timer.start()

    def graphic_6(self, graphic_level):
        if graphic_level != 5:
            self.graphic_5(graphic_level)
        self.message.name = "Message"
        self.message.value = "SECRET MESSAGE"
        self.encrypted.name = "Encrypted Message"
        self.message.add_value("")
        self.remove_flying_letters()
