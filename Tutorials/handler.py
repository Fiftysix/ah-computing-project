from PyQt5 import QtWidgets, uic, QtGui
import os
import functools
import extra


def list_tutorials(tutorial_dir):
    tutorials = []
    for dir_name in os.listdir(tutorial_dir):
        path = os.path.join(tutorial_dir, dir_name)
        if os.path.isdir(path) and os.path.exists(os.path.join(path, "info.conf")):
            tutorial = {"Title": "<Tutorial>", "Description": "<...>", "path": path}
            with open(os.path.join(path, "info.conf"), "r", encoding="utf-8") as info_file:
                for line in info_file.read().split("\n"):
                    if "=" in line:
                        name, value = line.split("=", 1)
                        tutorial[name] = value
            tutorial["thumbnail"] = QtGui.QPixmap(os.path.join(path, "thumbnail.png"))
            tutorials += [tutorial]
    return tutorials


class TutorialBase(QtWidgets.QWidget):
    def __init__(self, path):
        QtWidgets.QWidget.__init__(self)
        uic.loadUi('Interfaces/tutorial.ui', self)  # Load the .ui file

        self.text_widget = QtGui.QTextDocument()
        self.title_widget = self.findChild(QtWidgets.QLabel, "Title")
        self.page_widget = self.findChild(QtWidgets.QLabel, "Page")
        self.graphics_widget = self.findChild(QtWidgets.QFrame, "graphics")

        self.findChild(QtWidgets.QTextEdit, "textPoints").setDocument(self.text_widget)
        self.findChild(QtWidgets.QPushButton, "Next").clicked.connect(self.next_page)
        self.findChild(QtWidgets.QPushButton, "Back").clicked.connect(self.back_page)

        self.page = 1
        self.num_pages = 1
        self.path = path
        self.graphic_level = 0
        self.media = extra.Media(os.path.join(self.path, "media"))

    def setup(self):
        self.display_page(self.page)

    def set_text(self, text):
        text = "<ul><li>"+"</li><li>".join(text.split("\n"))+"</li></ul>"
        self.text_widget.setHtml(text)

    def display_graphic(self, number):
        getattr(type(self), "graphic_"+str(number))(self, self.graphic_level)

        self.graphic_level = number

    def display_page(self, number):
        self.page = max(min(number, self.num_pages), 1)
        print("Tutorial Page:", self.page)
        self.page_widget.setText(f"Page {self.page} of {self.num_pages}")
        self.set_text(open(os.path.join(self.path, "text/"+str(self.page)+".txt"), encoding="utf-8").read())
        self.display_graphic(number)

    def clear_graphic(self):
        layout = self.graphics_widget.layout()
        for i in range(layout.count()):
            layout.itemAt(0).widget().deleteLater()
            layout.itemAt(0).widget().setParent(None)

    def next_page(self):
        if self.page < self.num_pages:
            self.display_page(self.page+1)

    def back_page(self):
        if self.page > 1:
            self.display_page(self.page-1)


class TutorialHandler(QtWidgets.QWidget):
    def __init__(self, tutorial_dir):
        QtWidgets.QWidget.__init__(self)
        self.tutorials = list_tutorials(tutorial_dir)
        self.sub_widget = None
        self.setup()

        self.teaching = False  # if a tutorial is in progress
        self.tutorial = None

    def setup(self):
        layout = QtWidgets.QVBoxLayout()
        self.sub_widget = QtWidgets.QWidget()
        layout.addWidget(self.sub_widget)
        self.setLayout(layout)

    def set_main_widget(self, widget):
        self.layout().removeWidget(self.sub_widget)
        self.sub_widget.deleteLater()
        self.sub_widget = widget
        self.layout().addWidget(self.sub_widget)

    def display_buttons(self):
        layout = QtWidgets.QGridLayout()

        row = 0
        column = 0
        for tutorial in self.tutorials:
            button = QtWidgets.QPushButton()
            button.clicked.connect(functools.partial(self.load_tutorial, tutorial["Title"]))
            button.setObjectName("tutorialButton")
            button.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding))
            button_layout = QtWidgets.QVBoxLayout()
            button.setLayout(button_layout)
            image = extra.Image(tutorial["thumbnail"])
            image.setObjectName("tutorialImage")
            title = QtWidgets.QLabel(tutorial["Title"])
            desc = QtWidgets.QLabel(tutorial["Description"])
            button_layout.addWidget(image, 1)
            button_layout.addWidget(title)
            button_layout.addWidget(desc)
            layout.addWidget(button, row, column)

            column += 1
            if column == 3:
                row += 1
                column = 0

        new_widget = QtWidgets.QWidget()
        new_widget.setLayout(layout)
        self.set_main_widget(new_widget)

    def load_tutorial(self, title):
        print("Started Tutorial:", title)
        tutorial = None
        for t in self.tutorials:
            if t["Title"] == title:
                tutorial = t
                break

        code_path = os.path.join(tutorial["path"], "tutorial.py")
        code_file = open(code_path)
        program = compile(code_file.read(), code_path, "exec")
        variables = {"TutorialBase": TutorialBase, "Handler": self, "__name__": "Tutorial"}

        exec(program, variables)  # "variables" will be updated with variables resulting from file execution

        if "Message" in tutorial:
            message = tutorial["Message"]
        else:
            message = "SECRET MESSAGE"
        self.tutorial = variables["Tutorial"](message, tutorial["path"])
        self.set_main_widget(self.tutorial)

        self.teaching = True


if __name__ == "__main__":
    # Testing
    app = QtWidgets.QApplication([])
    handler = TutorialHandler(".")
    handler.show()
    app.exec_()
