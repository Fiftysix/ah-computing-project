# https://www.tutorialspoint.com/pyqt/pyqt_database_handling.htm
from PyQt5 import QtSql, QtWidgets, QtGui
from PyQt5.QtCore import Qt
import time
import types
import game as games


class Database:
    def __init__(self, location="scores.db"):
        self.location = location

        db = QtSql.QSqlDatabase.addDatabase('QSQLITE')
        db.setDatabaseName('scores.db')

        if not db.open():
            print("Failed to connect")
            return

        self.query = QtSql.QSqlQuery()

        self.setup()

    def execute(self, query):
        result = self.query.exec_(query)
        if not result:
            print("Error:", self.query.lastError().text())
            return False
        elif self.query.isSelect():
            values = []
            while self.query.next():
                values += [self.query.record()]
            return values
        else:
            return True

    def setup(self):
        # create class table
        self.execute("""
                CREATE TABLE IF NOT EXISTS Class (
                    name TEXT PRIMARY KEY
                );
                """)
        # create player table
        self.execute("""
                CREATE TABLE IF NOT EXISTS Player (
                    playerID INTEGER PRIMARY KEY,
                    className TEXT NOT NULL REFERENCES Class(name),
                    name TEXT NOT NULL,
                    isGroup INTEGER NOT NULL,
                    description TEXT
                );""")
        # create game table
        self.execute("""
                CREATE TABLE IF NOT EXISTS Game (
                    gameID INTEGER PRIMARY KEY,
                    name TEXT NOT NULL
                );
                """)
        # create score table
        self.execute("""
                CREATE TABLE IF NOT EXISTS Score (
                    score REAL NOT NULL,
                    gameID INTEGER NOT NULL REFERENCES Game(gameID),
                    playerID INTEGER REFERENCES Player(playerID),
                    className TEXT REFERENCES Class(name),
                    time INTEGER
                );""")

        for game in games.list_games("Quizzes", "Games"):
            if len(self.execute(f"""SELECT * FROM Game WHERE name = "{game["Title"]}" """)) == 0:
                print(game["Title"])
                self.execute(f"""
                                 INSERT INTO Game ("name")
                                 VALUES ("{game["Title"]}")
                            """)

    def add_class(self, class_name):
        return self.execute(f"""INSERT INTO Class ("name") VALUES ("{class_name}");""")

    def add_group(self, class_name, group_name):
        return self.execute(f"""
                                INSERT INTO Player ("className", "name", "isGroup", "description")
                                VALUES ("{class_name}", "{group_name}", 1, "");
                            """)

    def add_student(self, class_name, student_name):
        return self.execute(f"""
                                INSERT INTO Player ("className", "name", "isGroup")
                                VALUES ("{class_name}", "{student_name}", 0);
                            """)

    def add_member(self, class_name, group_name, name):
        members = self.execute(f"""
            SELECT description FROM Player
            WHERE className = "{class_name}" AND name = "{group_name}" AND isGroup = 1
        """)[0].field(0).value()

        if len(members) > 0:
            members += ","
        members += name
        self.execute(f"""
            UPDATE Player
            SET description = "{members}"
            WHERE className = "{class_name}" AND name = "{group_name}" AND isGroup = 1
        """)

    def remove_member(self, class_name, group_name, name):
        current_members = self.execute(f"""
            SELECT description FROM Player
            WHERE className = "{class_name}" AND name = "{group_name}" AND isGroup = 1
        """)[0].field(0).value()

        updated_members = []
        for member in current_members.split(","):
            if member != name:
                updated_members += [member]
        self.execute(f"""
            UPDATE Player
            SET description = "{",".join(updated_members)}"
            WHERE className = "{class_name}" AND name = "{group_name}" AND isGroup = 1
        """)

    def add_player_score(self, score, player_name, game_name, is_group):
        return self.execute(f"""
            INSERT INTO Score ("score", "gameID", "playerID", "time")
            VALUES ({score}, (SELECT gameID FROM Game WHERE Game.name = "{game_name}"),
            (SELECT playerID FROM Player WHERE Player.name = "{player_name}" AND isGroup = {int(is_group)}), {int(time.time())})
        """)

    def add_class_score(self, score, class_name, game_name):
        # TODO: replace existing score
        return self.execute(f"""
                                INSERT INTO Score ("score", "gameID", "className", "time")
                                VALUES ({score}, (SELECT gameID FROM Game WHERE Game.name = "{game_name}"), "{class_name}", {int(time.time())})
                            """)

    def search_scores(self, order, class_name=False, game_name=False, player_name=False):
        print(order, class_name, game_name, player_name)
        game_select = f"""AND Game.name = "{game_name}" """ if game_name else ""
        name_select = f"""AND Player.name = "{player_name}" """ if player_name else ""
        class_select = f"""AND Player.className = "{class_name}" """ if class_name else ""
        if order == "Name":
            order_by = "ORDER BY Player.name ASC"
        elif order == "Lowest First":
            order_by = "ORDER BY Score.score ASC"
        else:
            order_by = "ORDER BY Score.score DESC"

        students = self.execute(f"""
            SELECT Player.name AS "Player Name", Game.name AS "Game Name", Score.score AS "Score"
            FROM Player, Score, Game
            WHERE Player.playerID = Score.playerID AND Score.gameID = Game.gameID
                AND Player.isGroup = 0
                {class_select}{game_select}{name_select}
            {order_by}""")

        groups = self.execute(f"""
            SELECT Player.name AS "Player Name", Player.description AS "Members", 
                Game.name AS "Game Name", Score.score AS "Score"
            FROM Player, Score, Game
            WHERE Player.playerID = Score.playerID AND Score.gameID = Game.gameID
                AND Player.isGroup = 1
                {class_select}{game_select}{name_select}
            {order_by}""")

        class_ = []
        if class_name is not False:
            if order == "Name":
                order_by = "ORDER BY Score.score DESC"
            class_ = self.execute(f"""
                SELECT Game.name AS "Game Name", Score.score AS "Score"
                FROM Score, Game
                WHERE Score.gameID = Game.gameID
                    AND Score.className = "{class_name}"
                    {game_select}
                {order_by}""")

        return students, groups, class_

    def delete_player(self, player_name, is_group):
        self.execute(f"""
            DELETE FROM Score
            WHERE EXISTS (SELECT * FROM Player WHERE Player.playerID = Score.playerID
                AND Player.name = "{player_name}" AND Player.isGroup = {int(is_group)})
        """)
        self.execute(f"""
            DELETE FROM Player
            WHERE Player.name = "{player_name}" AND Player.isGroup = {int(is_group)}
        """)

    def delete_class(self, class_name):
        self.execute(f"""DELETE FROM Player WHERE Player.className = "{class_name}" """)
        self.execute(f"""DELETE FROM Class WHERE Class.name = "{class_name}" """)
        self.execute(f"""DELETE FROM Score WHERE Score.className = "{class_name}" """)
        self.execute(f"""DELETE FROM Score WHERE EXISTS 
                            (SELECT * FROM Player WHERE Player.playerID = Score.playerID
                                AND Player.className= "{class_name}")""")


class ResultDisplay(QtWidgets.QWidget):
    def __init__(self, parent, results, descriptions):
        QtWidgets.QWidget.__init__(self, parent, Qt.Dialog)

        layout = QtWidgets.QGridLayout()
        self.setLayout(layout)

        self.toolbox = QtWidgets.QToolBox()
        layout.addWidget(self.toolbox, 1, 0, 1, 2)
        sort_label = QtWidgets.QLabel("Sort by:")
        layout.addWidget(sort_label, 0, 0, 1, 1)

        self.sort_box = QtWidgets.QComboBox()
        self.sort_box.addItems(["Highest First", "Lowest First", "Name"])
        self.sort_box.currentTextChanged.connect(self.sort)
        layout.addWidget(self.sort_box, 0, 1, 1, 1)

        self.tables = []

        print("\nRESULTS\n")
        for result_idx in range(len(results)):
            result = results[result_idx]
            for record in result:
                for field_idx in range(record.count()):
                    print(record.field(field_idx).value(), end=", ")
                print()
            print()

            h = len(result)
            if h == 0:
                self.toolbox.addItem(QtWidgets.QLabel("No Results"), QtGui.QIcon(), descriptions[result_idx])
            else:
                w = result[0].count()

                table = QtWidgets.QTableWidget()
                table.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)

                table.setColumnCount(w)
                table.setRowCount(h)

                table.setHorizontalHeaderLabels([result[0].fieldName(idx) for idx in range(result[0].count())])

                for x in range(w):
                    for y in range(h):
                        item = QtWidgets.QTableWidgetItem(str(result[y].field(x).value()))
                        table.setItem(y, x, item)

                self.toolbox.addItem(table, QtGui.QIcon(), descriptions[result_idx])
                self.tables += [table]

        self.sort()

        print("\nEND\n")

        self.show()

    def sort(self):
        by = self.sort_box.currentText()
        print("sort by", by)

        for table in self.tables:
            row_amount = table.rowCount()
            column_amount = table.columnCount()
            rows = []
            for row_idx in range(row_amount):
                rows += [[]]
                for column_idx in range(column_amount):
                    rows[-1] += [table.item(row_idx, column_idx).text()]

            if by == "Highest First":
                rows = sort(rows, lambda row: 0-float(row[-1]))
            elif by == "Lowest First":
                rows = sort(rows, lambda row: float(row[-1]))
            else:
                rows = sort(rows, lambda row: row[0])

            for row_idx in range(row_amount):
                for column_idx in range(column_amount):
                    table.item(row_idx, column_idx).setText(str(rows[row_idx][column_idx]))


# Bubble Sort
def sort(lst, key):
    for i in range(len(lst)):
        j = i
        while j > 0:
            j -= 1
            if key(lst[j]) > key(lst[j+1]):
                # swap values
                lst[j+1], lst[j] = lst[j], lst[j+1]
            else:
                break

    return lst
