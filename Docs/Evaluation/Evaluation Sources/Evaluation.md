# ________________________________________________________________________________________________





# Evaluation



# ________________________________________________________________________________________________





## Evaluation

This evaluation report evaluates each section of the project, including a comparison of the requirements and final result, and the results of testing. It also discusses things which were missed, and why.

##### Design

The design of the program closely followed the decisions from the analysis stage.

The design outlined every aspect of how the project should be implemented.

The only part from the analysis that was not in the design was the game's content, which were implemented with limited previous designing.

##### Implementation

The implementation took a lot longer than initially planned, and I also took a break from the project for a short time, resulting in the project being delayed greatly.

Most features in the design phase were implemented into the final program:

-   The theme selection menu was not implemented as shown, and is instead a much more basic menu mainly to prove that themes are supported.
-   Games were implemented, mostly following the design, But were somewhat simplified from what I had imagined.
-   The option to open a new window was left out, as the program could simply be re-run to achieve the same result.

##### Testing

Most of the testing was done at the end of the implementation, instead of throughout it. This resulted in most tests passing the first time, and earlier errors not being recorded. Some of the earlier testing was written down later from memory.

The module testing showed that nearly every aspect of the program that was implemented worked correctly.

The requirements testing showed that one feature of quiz printing was missed (including answers for marking).

No testing with a persona or test cases was done as this was not possible due to the covid-19 situation, and school not being open. Due to this it is likely that some problems with using the program, and ease of use of the interfaces were not identified, and were not fixed. Because of this it may be useful to do this testing, and improve the software if it was ever to be used in education.

##### Maintainablility

The program was implemented under some time constraints, meaning not all the code was implemented to the best standards. Some code was rushed, and would be hard to maintain, including duplicated code. Other parts were well written and modular. These parts would be much easier to maintain. Another issue is that large parts of the program are badly documented, meaning more time would be required to understand the implementation of the program.

Tutorials, Quizzes and Games can all be added or removed without modifying other sections of the program, allowing these to be updated or distributed individually. Some content, such as the Quizzes, can be created or modified without any programming knowledge, since they are read entirely or partially from text files or xml files.

##### Time

During the analysis I was very optimistic about how much could be achieved, but during the design stage I could tell that the project was larger than I first thought, mainly due to the quizzes and tutorials being repeated  6 times. During the implementation stage, I managed to implement most of what I had planned, although the 2 games had been mostly left out from the design stage, and implementation was left till very late. The main reason for the project to be finished so early was that it could be tested in school in a maths class, which could not be completed due to there not being any school, therefore this was not such an issue.

##### Conclusion

The project was completed, with nearly everything implemented as the design stated, and somewhat maintainable. The usage of the project could not be tested, which quite likely reduced the quality of the final program.