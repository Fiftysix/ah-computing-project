# Gannt chart

```mermaid
        gantt
        dateFormat  YYYY-MM-DD
        title Proposed project plan

        section Analysis
        Analysis : 2019-11-25,14d

        section Design
        Research encryption and similar projects : 7d
        Assess feasibility : 7d
        Design program structure : 21d
        Design interfaces : 14d
        Design database and queries : 2020-01-20,7d

        section Implementation
        Implement program : 2020-02-03,21d
        Compare program to design : 2020-02-17,7d

        section Testing
        Create test plan : 2020-01-27,7d
        Test program : 2020-02-03,21d
        
        section Evaluation
        Evaluation : 7d
```

The time required to create the project although took longer. I was not able to complete the project in the proposed time.

I also took a break from the project near the end of march, causing extra delay.

```mermaid
        gantt
        dateFormat  YYYY-MM-DD
        title Actual time taken

        section Analysis
        Analysis : 2019-11-25,14d

        section Design
        Research encryption and similar projects : 7d
        Assess feasibility : 7d
        Design program structure : 21d
        Design interfaces : 14d
        Design database and queries : 7d

        section Implementation
        Implement program : 2020-02-03,2020-04-13
        Compare program to design : 2020-03-30,7d

        section Testing
        Create test plan : 2020-04-13,7d
        Test program : 2020-03-30,21d
        
        section Evaluation
        Evaluation : 2020-04-13,14d
```