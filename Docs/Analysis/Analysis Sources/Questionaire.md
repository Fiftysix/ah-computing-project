# Questionnaire

- Who the software will be used by (age, skill)
- How the software will be used (on board, individual computers)
- What the project should contain (difficulty level, type of content, examples from books)
Simple ciphers

What type of thing I am suggesting...

- Who will use the software?
	- What level?
>   S1-3 

> Mainly taught by software.  

- Will the software be used by just the teacher (on the board), or on individual computers by students?
> Board  

- Will there be lots of student interaction?
> Highly interactive   

- How complicated should it be? Should it be more focused on fun or learning?
> educational  

- How focused on the mathematics of it?
> More visual than mathematical  

- Types of things there could be:
	- Games?
	- Tutorials?
	- Visualisations/Animations? 
> Very aesthetically pleasing, easy fonts, cartoons, colours, many animations, easily accessible (dyslexia, etc)  
	
	- Printable documents?

> Maths patterns and sequences in 3rd level maths  
> Substitution and transposition ciphers  

> Printable versions  

- Will be taught about February/march
- Will be tested with S1, they will be interested.
