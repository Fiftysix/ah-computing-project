# Analysis

## Description of the problem
The purpose of this program is to teach young people in S1-S3 about different methods of basic cryptography in a classroom environment. The program will include highly interactive visual tutorials and games, which will be displayed on a smart-board at the front of the class, and also allow worksheets to be created and printed in order for students to work individually or in smaller groups.
This program has the ability to be developed in all three AH topics: Web development, Software Development and Databases.
#### Scope:

The program needs to include:
- Easy to use interfaces, including:
	- Bright colours
	- Easy to read, large fonts
	- Graphics
	- Animations
- Printable versions of quizzes
- Input of student's results, including input validation
- Very interactive content
The games and tutorials should focus on being educational, but more visual explanations than mathematical explanations.

#### Boundaries:
- The software will only be used by S1-S3 students
- The teacher will always be available to help or configure activities
- Student results will be stored in a local database

## UML use case diagram
*UML diagram on next page*

## Requirements specification

#### End user requirements:
Users expect to:
- Select different options for games & tutorials
- Have a variety of different ciphers to choose from
- Customise then print worksheets
- Enter results of worksheets into the program
- Cooperate when using the program

#### Functional requirements:
- The program should run on Windows 10 OS
- The program should store students scores in a database
- The interface should give basic options to select when the program is started
- The program can be installed and executed easily

## Project plan
**Tasks to be completed:**
- [ ] Create this document
- [ ] Research encryption methods, similar projects, development options
- [ ] Write feasibility study
- [ ] Design program structure
- [ ] Design interfaces for each component
- [ ] Design databases
- [ ] Design SQL queries
- [ ] Ensure end-user and functional requirements are met in design
- [ ] Create test plan
- [ ] Research required skills
- [ ] Implement program
- [ ] Ensure program matches design
- [ ] Regularly test program during development, and log results
- [ ] Collect screenshots of interfaces
- [ ] Test with class
- [ ] Create evaluation report

To develop and test the program, the following could be used:

- Software development:
	- Python, VB or C#
- Web development:
	- PHP, HTML, Javascript
- Database:
	- MS Access Forms, VB, MySQL, SQLite

*Gannt chart on next page*