# Feasibility Study


The feasibility study is a document created between the analysis and design stages of a project. The study aims assess whether it is feasible to complete the project given the economic, time, legal and technical limitations.

## Economic

This section explains why the project is economically feasible or not feasible.
Usually, this section would discuss the costs required to pay people to develop the project, as well as the costs of the resources required.
I however will be developing the project by myself, meaning there will be no cost for a developer.
I have the resources required to develop the project already available to me, and the resources required to test the project are supplied by the school.
The only cost of this project is from printing, which will be covered by the school.


## Time
This section explains why the project can feasibly or not feasibly be completed within the required time.
As shown in the gannt chart, the project is proposed to be ready by the end of February, which was a requirement of the client. The gannt chart gives more time than should be necessary (float. Read notes), meaning the project will most likely be completed within the specified time. The project is required to be used in February or March, which the project will most likely be finished in time for.


## Legal
Personal data of pupils (names, classes) will be stored in a database on a local drive of the teacher's computer.
In order to secure the database, a password could be used, which the teacher knows, so only the teacher can access it, or the password is stored in the program, meaning other software has no access to it. This may not be necessary though, as the location the database is stored in is already password protected by default.
Students will not have unsupervised access to the application in order to act maliciously (eg. SQL injections), but since the students are no older than S3, they will most likely not have the skill for this to be an issue.
Due to these reasons, the project has no GDPR implications.

All media will be created by myself or a colleague, or will be supplied as royal free.
Software libraries used in this project will have open source licences.
This means there are no issues with the copyright and patents act


## Technical
The program will run on existing computers running windows 10 and connected to an existing projector. No new hardware is needed.

Software used to develop the program must run on the existing computers. I also have to be able to use this software efficiently without spending time learning new software.
The language I am most familiar with is Python, and the GUI framework I am familiar with is PyQT, wich has many features necessary to create the program, and includes an interface designer suitable for developing the programs interfaces.

Users of the system will not require much training, as the software will not have many features, and aims to be easy to use.