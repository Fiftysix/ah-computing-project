# Project proposal

I want to create a program to demonstrate and teach basic encryption methods to students in maths.

I would like to create a graphical interface featuring visualisations of basic encryption methods. It will be aimed at informing and teaching younger students. It could include tutorials, examples, and games.

The project should integrate with a database, which might store  game or quiz results, or progress in a tutorial.

Suitable software development tools and methods will be chosen to complete the project.