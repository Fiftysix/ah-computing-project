# Test plan

# Requirements Testing

#### End user requirements:

**The following should be available**

|                                                              | Available? | Notes                               |
| ------------------------------------------------------------ | ---------- | ----------------------------------- |
| Different options to choose games or tutorials               | Yes        |                                     |
| A variety of different ciphers                               | Yes        |                                     |
| Customising and printing of worksheets                       | Partially  | worksheets not customisable         |
| Entering and storing results of quizzes/games into the program | Yes        |                                     |
| Features allowing cooperation when using the program         | Yes        |                                     |
| An easy to use interface                                     | Yes        | Large buttons and fonts, clear text |



#### Functional requirements:

**The following should be implemented and work:**

|                                                              | Implemented? | Notes                            |
| ------------------------------------------------------------ | ------------ | -------------------------------- |
| Running the program on Windows 10 OS                         | Yes          | Not tested in school environment |
| storing students scores in a database using the program      | Yes          |                                  |
| An interface giving basic options to select once the program is started | Yes          |                                  |
| Installation and executing/launching the program easily      | No           |                                  |

  

# Module Testing

#### GUI

| **Test**    | **Description**                                              | **Test type**     | **Initial Test** | **Final Test** |
| ----------- | ------------------------------------------------------------ | ----------------- | ---------------- | -------------- |
| Open window | A window should be displayed which contains the content of the application | Functional/Visual | Pass             |                |



#### Home Screen

| **Test**      | **Description**                       | **Test type** | **Initial Test** | **Final Test** |
| ---------------- | ------------------------------------- | ------------- | ---------------- | -------------- |
| Sidebar Contents | Sidebar lists correct tutorials/games | Visual        | Pass |                |
| Sidebar toggle   | The sidebar can be toggled using a button | Visual | Pass |                |
| Buttons | 3 buttons should be displayed, and should take the user to the correct page when clicked | Visual | Pass |                |



#### Game selector

| **Test**      | **Description**                          | **Test type** | **Initial Test** | **Final Test** |
| ---------------- | ---------------------------------------- | ------------- | ---------------- | -------------- |
| Load Information | Load information about games and quizzes | Value         | Pass |                |
| Display Games | Display buttons for each game or quiz    | Visual        | Pass |                |
| Display Thumbnails | Load and display thumbnail image for each game | Visual | Pass | |
| Start Game/Quiz  | Open correct quiz when button pressed  | Functional    | Pass |                |



#### Tutorial selector

| **Test**      | **Description**                          | **Test type** | **Initial Test** | **Final Test** |
| ---------------- | ---------------------------------------- | ------------- | ---------------- | -------------- |
| Load Information | Load information about tutorials         | Functional | Pass |                |
| Display Tutorials | Display buttons for each tutorial        | Visual | Pass |                |
| Display Thumbnails | Load and display thumbnail image for each tutorial | Visual | Pass | |
| Start Tutorial   | Run correct tutorial when button pressed | Functional | Pass |                |



#### Options

| **Test**     | **Description**                             | **Test type** | **Initial Test** | **Final Test** |
| --------------- | ------------------------------------------- | ------------- | ---------------- | -------------- |
| Change Theme    | Change the colour theme of the interface    | visual        | Not Implemented | Pass |
| Search Database | find and display scores relevant to a query | functional    | Pass |                |
| Edit Database   | add and remove students, classes and groups | functional    | Pass |                |



#### Database

| **Test** | **Description**                         | **Test type** | **Initial Test** | **Final Test** |
| ----------- | --------------------------------------- | ------------- | ---------------- | -------------- |
| Connect     | Successfully connect to a database file | functional    | Pass |                |
| Read        | Query the database and return values    | functional    | Pass |                |
| Write       | Add or modify records in the database   | functional    | Pass |                |



#### Quizzes

| **Test** | **Description** | **Test type** | **Initial Test** | **Final Test** |
| ----------- | --------------- | ------------- | ---------------- | -------------- |
| Load Quiz | Load quiz questions | Functional | Pass |                |
| Display Questions | Display Question text and images | Visual | Pass | |
| Display answers | radio buttons or a text box should be displayed depending on the type of question | Visual | Pass | |
| Check Answers | the correct decision about the player's answer is made | Functional | Pass | |
| Calculate score | The players score is calculated based on the answer. | Functional | Pass | |
| Print Quiz | Generate a pdf containing the quiz questions and display a print dialogue | Functional | Large images are cut off | Pass |
| Input Scores | Obtain valid scores from the user for a group, class or individual | Functional | Pass | |



#### [individual games]

###### Encryption game

| **Test** | **Description** | **Test type** | **Initial Test** | **Final Test** |
| ----------- | --------------- | ------------- | ---------------- | -------------- |
| Load Game | Game program is loaded | Functional | Not implemented | Pass |
| Start Game | Game program is executed | Functional | Not implemented | Pass |
| Run game | Game runs as intended | Functional/Visual | Not implemented | Pass |
| Calculate score | The players score is calculated. | Functional | Not implemented | Pass |

###### Decryption game

| **Test** | **Description** | **Test type** | **Initial Test** | **Final Test** |
| ----------- | --------------- | ------------- | ---------------- | -------------- |
| Load Game | Game program is loaded | Functional | Not implemented | Pass |
| Start Game | Game program is executed | Functional | Not implemented | Pass |
| Run game | Game runs as intended | Functional/Visual | Not implemented | Pass |
| Calculate score | The players score is calculated. | Functional | Not implemented | Pass |



#### [individual tutorials]

###### General

| **Test** | **Description** | **Test type** | **Initial Test** | **Final Test** |
| ----------- | --------------- | ------------- | ---------------- | -------------- |
| Text | Text for each page is loaded from a file and displayed. | Visual | Pass |                |
| Buttons | "Next" and "Back" buttons go forwards and backwards through the tutorial | Functional | Pass | |

###### Caesar Cipher

| **Test** | **Description** | **Test type** | **Initial Test** | **Final Test** |
| ----------- | --------------- | ------------- | ---------------- | -------------- |
| Graphics | Graphics are loaded and displayed | Visual | Pass |                |
| Animations | Animated diagrams are displayed. | Visual | Pass | |
| Encryption/Decryption | Text correctly encrypted and decrypted | Functional | Pass | |
| Explanation | Encryption and decryption methods are clearly explained | Theoretical | Pass | |

###### Atbash Cipher

| **Test** | **Description** | **Test type** | **Initial Test** | **Final Test** |
| ----------- | --------------- | ------------- | ---------------- | -------------- |
| Graphics | Graphics are loaded and displayed | Visual | Pass |                |
| Animations | Animated diagrams are displayed. | Visual | Pass | |
| Encryption/Decryption | Text correctly encrypted and decrypted | Functional | Pass | |
| Explanation | Encryption and decryption methods are clearly explained | Theoretical | Pass | |

###### Pigpen Cipher

| **Test** | **Description** | **Test type** | **Initial Test** | **Final Test** |
| ----------- | --------------- | ------------- | ---------------- | -------------- |
| Graphics | Graphics are loaded and displayed | Visual | Pass |                |
| Animations | Animated diagrams are displayed. | Visual | Pass | |
| Encryption/Decryption | Text correctly encrypted and decrypted | Functional | Pass | |
| Explanation | Encryption and decryption methods are clearly explained | Theoretical | Pass | |

###### Vigenère Cipher

| **Test** | **Description** | **Test type** | **Initial Test** | **Final Test** |
| ----------- | --------------- | ------------- | ---------------- | -------------- |
| Graphics | Graphics are loaded and displayed | Visual | Pass |                |
| Animations | Animated diagrams are displayed. | Visual | Pass | |
| Encryption/Decryption | Text correctly encrypted and decrypted | Functional | Pass | |
| Explanation | Encryption and decryption methods are clearly explained | Theoretical | Pass | |

###### Polibius Cipher

| **Test** | **Description** | **Test type** | **Initial Test** | **Final Test** |
| ----------- | --------------- | ------------- | ---------------- | -------------- |
| Graphics | Graphics are loaded and displayed | Visual | Pass |                |
| Animations | Animated diagrams are displayed. | Visual | Pass | |
| Encryption/Decryption | Text correctly encrypted and decrypted | Functional | Pass | |
| Explanation | Encryption and decryption methods are clearly explained | Theoretical | Pass | |

###### Route cipher

| **Test** | **Description** | **Test type** | **Initial Test** | **Final Test** |
| ----------- | --------------- | ------------- | ---------------- | -------------- |
| Graphics | Graphics are loaded and displayed | Visual | Pass |                |
| Animations | Animated diagrams are displayed. | Visual | Pass | |
| Encryption/Decryption | Text correctly encrypted and decrypted | Functional | Pass | |
| Explanation | Encryption and decryption methods are clearly explained | Theoretical | Pass | |
