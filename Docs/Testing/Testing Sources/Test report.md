# Log of testing

Components of the program were tested throughout the implementation stage. 

Below are details and results of some issues I came across.



**Date:** Feb 25

**Description of issue:** The labels displaying the messages and keys during tutorials were often not entirely visible, as the did not wrap their text due to there not being enough space.

**Resolution:** The font size was made to change with the amount of space available. It was calculated based off the width of the window, and updated when it changed size.



**Date:** March 15

**Description of issue:** Animated letters that moved across the screen did not match the font size of their source and target

**Resolution:** the `ValueLabel`s and the animated cipher visualisations were modified to include font sizes when returning information about the position of a specific letter.

**References:** N/A



**Date:** March 17

**Description of issue:** Non-alphabetical characters didn't have steps between source and target when being animated, as they are not encrypted. This caused them to reach the target in much shorter time than other letters, and end up in the wrong position.

**Resolution:** The time it would take for the one step these letters made was multiplied by the number of steps other letters made, causing them to reach the destination in the same time other letters did.



**Date:** March 18

**Description of issue:** Animated letters moved to and from the wrong positions due to the position calculations using the default font instead of the font applied by the theme

**Resolution:** A widget, which was on the screen, and therefore had the theme's style applied had to be used for the font. Also, it's font attribute had to be first updated with the font the widget had inherited. The font object could then be used instead of the default font.

**References:** https://doc.qt.io/qt-5/qstyle.html#polish



**Date:** March 25

**Description of issue:** When reading text files for the title and content for tutorials and quizzes belonging to the vigenère cipher, the character `è` was displayed as an invalid character.

**Resolution:** The encoding was specified as `UTF-8` when reading the text files.

**References:** https://stackoverflow.com/questions/147741/character-reading-from-file-in-python



**Date:** March 28

**Description of issue:** When developing the quiz printing, I needed to create check boxes in the document for the user to select the correct answer for a multiple choice question. The function to add content to the document using HTML however didn't support check boxes or CSS.

**Resolution:** Unicode characters representing a box were placed at the beginning of each answer. When printed, this looked like a normal check box.



**Date:** March 30

**Description of issue:** When the user's answer during a quiz was compared to the correct answer, a partially incorrect answer would get a too high score. This cause incorrect answers to still pass, because they still got a score above 0.

**Resolution:** The score was modified using the calculation `max(0, score*2-1)`. This resulted in only higher scores having an above-zero, allowing them to pass.



**Date:** April 5

**Description of issue:** The interface for adding scores to a player, or searching scores of a player do not give the option to specify the class for that player as well as the player's name. This was an issue as the database required the class when adding or searching player scores in the database.

**Resolution:** The database was first searched to find out what class a player belonged to. The class could then be used in the next query.



**Date:** April 26

**Description of issue:** Images didn't scale to fit fit inside their widget, and didn't scale when the window/content was resized. Qt doesn't supply an easy way to display an image.

**Resolution:** I created a custom class that draws an image at the correct position and scale, and redraws it every time the widget is resized.



**Date:** April 27

**Description of issue:** All images in the program were being successfully displayed during development of the project, but when the program was tested on Windows, some of the images were not visible.

**Resolution:** Some images were JPEG images with a `.png` file extension. These images were opened in GIMP, then re-exported in the PNG format.



**Date:** April 30

**Description of issue:** QAbstractScrollArea did not supply an option to keep the view scrolled to the bottom as more content was added to it. This meant that during the games, the story was not visible unless the user manually scrolled down.

**Resolution:** The order in which new content was added to the view was changed, so that new content appeared at the top, and the view did not need to be scrolled to see the newest content.

**References:** https://doc.qt.io/qt-5/qabstractscrollarea.html#SizeAdjustPolicy