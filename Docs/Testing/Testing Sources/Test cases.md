# Testing with persona and test cases

It was not possible to test with a persona and test cases, Therefore this section cannot be completed.

If it was possible, the following would be tested:

-   Ease of use of the program in general
-   Ease of installing the program
-   How students work together to solve questions
-   How easily the application can be seen from the student's perspective
-   How much input is required from the teacher
-   How much of the tutorial the students remember