# Algorithm design


### 1. Display tutorial/game buttons
```
Function (list of names, list of descriptions, list of images)
	for each name + description + image
		create new button
		add layout to button  # (allows adding of multiple widgets inside the button) #
		add image to button layout
		add name in large font to button layout
		add description in smaller font to button layout
	end
end
```

### 2. Load media

```
Function (media_path)
	media = []
	file_list = list all files in directory
	for each file in file_list do
		if file is an image then
			open file
			read and convert image
			add image to media[]
			close file
		end
	end
	return media[]
End
```


### 3. Load game/quiz/tutorial information
```
Function (content_path)
	info = []
	load media(content_path/thumbnails)  # (algorithm 2) #
	list all directories in content_path
	for dir in each directory do
		current_info = {name="", description="", path="", thumbnail=""}
		open file dir/info.txt
		current_info.name = read line 1 of file
		current_info.description = read line 2 of file
		current_info.path = dir
		image_name = read line 3 of file
		current_info.thumbnail = media[image_name]
		add current_info to info
	end
	return info
End
```


### 4. Generate PDF
```
Function (questions[])
	create graphics view
	add vertical box layout to view  # (aligns all widgets vertically) #
	for q in questions
		add label(q.question) to layout
		if q.multi-choice then
			for each q.choices
				add label(choice) to layout
			end
		end
		add space to layout
	end
	apply theme to layout
	generate pdf from graphics view
	return pdf
End
```


### 5. Display available themes
```
Function ()
	list all files in ./themes
	for theme in files
		# create preview for theme
		create view
		add title label(theme.filename) to view
		add label view("Small Text Preview...") to view
		open file theme.path
		view.set-theme(file.read())
		view.on-click = (function:
				find application base view
				set theme of base view
			end)
	end
End
```


### 6. Load quiz questions
```
Function (quiz_path)
	open file quiz_path/questions.xml
	questions = []
	for each element as question in parse-xml
		data = {q="", a="", multi-choice=false, wrong-answers=[]}
		data.q = question.question
		data.a = question.answer
		data.multi-choice = question.multi-choice
		if data.multi-choice then
			data.wrong-answers = question.wrong-answers
		end
		add data to questions[]
	end
	return questions
End
```


### 7. Database
```
Function (query)
	connect to database
	execute query
	if error:
		return error messages
	else
		return query result
	end
	close connection
End
```

