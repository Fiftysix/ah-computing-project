# Design


## Class design 
**(accompanying UML class diagram)**

- **Home screen**
	- multiple buttons to select
		- Tutorial - opens Tutorial selector
		- Game - opens game selector, includes printable tasks
		- Options - view, clear and export scores, change display options
	- Home screen handled by main class.

- **Tutorial**
	- tutorial class for handling tutorial class and interface/animations - different methods/styles to avoid repetition
	- Each tutorial has separate class describing it's steps, in separate python file
	- Each tutorial has media (text, graphics) stored in separate folders
	- Tutorial handler class to load and list tutorials
		- Creates instances of tutorial base class and each separate tutorial class

- **Game/Quiz**
	- Games and Quizzes both under games menu
	- Base quiz class loads questions and media, creates pdf files for printing, or displays individual questions on screen
	- Questions/answers stored in xml files
	- Interface for entering individual/team student scores from printed quizzes

	- Base game class, handles basic things, such as scores
	- Individual game classes handles the rest, including ui, as each game should be fairly different
	
	- Game and quiz handler loads and lists all games and quizzes
	- Database class - retrieves and adds scores to database


- **Misc**
	- Media class loads different types of media from a directory. Can be used by any other section.
	- Base GUI class handles fonts, themes and supplies methods to ensure consistency throughout the program. Inherited by all classes that implement GUIs
	
- **Options**
	- Change font, colours, ui scale
	- View/manage database


## Ciphers
> Every cipher has a tutorial  
+ Caesar Cipher
- Atbash Cipher
- Pigpen
- Vigenère Cipher
+ Polibius
- Route cipher

#### Game ideas:
1. **Game 1**
- You need to send a secret message to some of your friends
- Each friend has arranged multiple different ciphers to communicate with you.
- The messengers you send with the messages each know some de-ciphering, but they are not allowed to read the message.
- The aim is to encipher the message for each recipient so that no one along the way can read it.
- Each receiver is worth a number of points, depending on difficulty.
- If you choose a cipher the messenger can read, all points of the receiver.
- If you choose a cipher the receiver can't read, you have to retry.
- If you incorrectly encipher a message, you have to retry.
- Each time
- You can only have 3 attempts per receiver. After the third attempt you have lost all points available for that receiver.


2. **Game 2**
- You are a spy messenger. You must decipher a series of messages you are dilivering, encrypted with different cyphers (for security), to reveal an evil plan and stop it in time.

> One game with enciphering, one deciphering.  
> One game with delivering, one stealing.  

#### Quiz ideas
- Enciphering messages
- Deciphering messages
- Multiple choice of cipher rules/steps
- Multiple choice enciphering/deciphering (easy version)
- Mix of above


**Unused research:**
* Playfair Cipher 
+ Rail Fence


## Database design
- Classes
- Players  (people or groups)
- Scores
- Games


#### Queries:

- Add class
```
INSERT INTO Class ("Name") VALUES ({class-name})
```

- Add group
```
INSERT INTO Player ("ClassName", "Name", "IsGroup", "description") VALUES ({class}, {name}, true, {members})
```

- Add student
```
INSERT INTO Player ("ClassName", "Name", "IsGroup") VALUES ({class}, {name}, false)
```

- Add score
```
INSERT INTO ("Score", "GameID", "PlayerID", "Date") VALUES 
( {score}, {game-id},
	(SELECT "PlayerID" FROM Player WHERE Name = {player-name}),
{date} )
```

- Get class scores
```
SELECT Player.Name, Player.Description, Score, Game.Name
FROM Player, Score, Game
WHERE Score.PlayerID = Player.PlayerID AND Score.GameID = Game.GameID
[AND Player.IsGroup = {is-group}]  # optional
[AND Game.Name = {game}]  # optional
AND ClassName = {class}
ORDER BY Score DESC
```

- Delete player
```
DELETE FROM Score, Player
WHERE Score.PlayerID = Player.PlayerID AND Player.Name = {name}
```

- Delete class
```
DELETE FROM Score, Player, Class
WHERE Score.PlayerID = Player.PlayerID AND Player.ClassName = Class.ClassName AND Class.ClassName = {class}
```

## Design decision Justifications
### PDF
An option to save a quiz as a PDF, allowing it to be printed will be supplied.
When a quiz is shown on the computer screen, the whole class must work together on it. Though this is not a negative thing, the PDF allows students to work on quizzes individually, or in smaller groups. They can also be given to students who prefer a paper copy, or can be given as homework.

### Games
Students, especially younger ones, may get bored with too many quizzes. The games aim to add a fun aspect to the lesson.
There will only be 2 games included, as creating games will take lots of extra time. Mr McDonald (the client) has requested that one game is based on encryption, and one on decryption, though I aim to not make them too similar in order to keep them interesting.

### Theme changing options
Since the software may be used by a large variety of younger students, the interface must be easy to read and interesting.
Since I am not very familiar with what the best amount of colour and sizes are, I will supply at least 2 different themes to allow the teacher to choose the best option.
I will also allow an option to change to the operating system's default theme to make development easier.

### Sidebar
The sidebar gives easy access to any section of the application, meaning you can navigate without returning to the homepage.
The sidebar can be hidden to give a full screen view.

### Database
The database gives a more advanced layout for storing student's scores. It allows separating students into their classes, allowing the teacher to load pre-entered names of his current class when they complete a game or quiz. The database also allows scores to be entered for a group of students, or the whole class.
The storage of scores allows the teacher to keep track of how well students are learning, and to keep track of homework.