# Research

*Research source: https://www.braingle.com/brainteasers/codes .*

## Substitution Ciphers
A substitution cipher is a cipher where each letter is replaced by another letter.

**The following are well known substitution ciphers:**

#### Mono-alphabetic Ciphers:

A mono-alphabetic cipher makes the same change for each letter in the message. These can easily be cracked, because once one letter is known, the rest of the message can easily be read.
- Caesar Cipher
- Atbash Cipher
- Keyword Cipher
- Pigpen / Masonic Cipher
- Polybius Square .

#### Poly-alphabetic Ciphers
In a poly-alphabetic cipher, a letter may be encoded differently at different points in the message, making it harder to crack.
- Vigenère Cipher 
* Beaufort Cipher 
* Autokey Cipher 
* Running Key Cipher 

#### Poly-graphic Ciphers
In poly-graphic ciphers, letters are substituted in groups instead of individually.
* Playfair Cipher 
* Bifid Cipher 
* Trifid Cipher 
* Four-square cipher 


I have chosen to research some of the above, which may be useful in my project:

### Caesar cipher
- All letters are shifted along in the alphabet a certain number of spaces
- To decrypt, the letters are shifted back the same amount.
There are only 25 possible combinations, making it easy to decrypt without knowing the correct shift.

### Atbash Cipher
- Each letter is replaced by the letter with it's position in the alphabet counted from the other end of the alphabet
- Basically, the alphabet is replaced with it's reverse. So A->Z, C->X.
- To decrypt the message, the exact same process is repeated.
Anyone can decrypt a message with this cipher simply by knowing which cipher was used, as there is only one combination.

### Pigpen Cipher
- Each letter is replaced by a symbol
- The symbol for a letter can be found by taking the lines surrounding a letter from the image below:
![](images/1.png)
- To decrypt, each symbol is replaced by it's matching letter using the same image.

### Polybius Square

- Create a 5x5 table of the alphabet  (combine i and j)
- Replace each letter of the message with the row and column numbers (H -> row 2, column 3 -> 23)
- To decipher, split the message into pairs of numbers
- Replace each pair of numbers with the letter at the corresponding row and column

### Vigenere Cipher
- A keyword is chosen, which is repeated to the same length of the message (message: "secret message", key: "alphab etalpha")
- The intersection between each message letter and keyword letter is found in the grid shown below:
![](images/2.png)
- The letter at the intersection becomes the encoded letter
- To decipher, for each letter in the keyword, the row is followed until the message letter is found
- The column is then followed up, where the original letter is the first row of the table.

### Playfair cipher
This cipher relies on pairs of letters rather than individual letters, making it stronger
-  A keyword is chosen, and a table is created using the keyword and the alphabet (removing duplicate letters), to create a 5x5 table. Example:
![](images/3.png)

- The message is grouped into pairs of letters (se cr et te xt)
- The following rules are applied to each pair:
1. If 2 letters are the same, add an X between them
2. If the last pair only has one letter, an X is added
3. If 2 letters are on the same row of the table, replace them with the letter to their right (wrap to start)
4. If 2 letters are on the same column, replace them with the letter below (wrap to top)
5. If the 2 letters are on different rows and columns, swap only the columns of the letters.

- To decipher, do points 3 and 4 in reverse, then do point 5 normally.



## Transposition ciphers

**The following are well known Transposition ciphers:**
- Rail Fence
- Route Cipher
- Columnar Transposition


I have chosen to research one example, which may be useful for my project:

### Rail Fence Cipher
- The message is written vertically in equally sized columns.
- This results in horizontal "rails".
- The message is then read horizontally.
- To decipher the message, it is split evenly into the same number of rows ("rails") as before, and is then read vertically.

### Route Cipher (twisted path?)
- Write the message vertically in columns of any size
- Draw a path through the grid, connecting every letter
- Read the message along the path 
- To decipher the message, draw a grid, and follow the path, filling in each position with the next letter of the message.
- Read the message in columns



## Similar Projects

https://www.giftofcuriosity.com/secret-codes-for-kids/
Contains printable document: https://www.giftofcuriosity.com/product/codes-and-ciphers-fun-pack/