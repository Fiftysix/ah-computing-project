# Tutorial designs


## Caesar Cipher
> Should attempt to use circular visualisations.  
1.  
	- The Caesar Cipher is named after Julius Caesar, who used it for his private messages.
	- [show image of caesar cipher wheel]
2. 

	- Choose a message you want to encipher (eg. "SECRET MESSAGE")
	- Choose a number (between 1 and 25) to use as your key (eg. 6).
[display values at top of display area]
2. 

	- Write the alphabet in a line
	- Write the alphabet again, below the first line, but using your key as the position to start at
	- Once the second alphabet reaches the end of the first alphabet, go back to the beginning.
[display animation of alphabets being drawn, animation moving end of second alphabet to beginning]
3. 

	- Go through each letter in the message, and find it in the top row
	- Write down the letter below the top letter
[animation showing movement from original message through the alphabets, creating the new message]
4. 

	- Encryption complete!
	- Your enciphered message is "YKIXKZ SKYYGMK"
[highlight encrypted message]
5. 

	- To get back to your original message, simply search for each letter in the bottom row instead, and read the letter in the top row.
[remove original message. Then same animation as step 3, but reversed]
6. 

	- Summary
	- Write 2 alphabets. One with a chosen offset
	- Find each letter of the message in the top alphabet, and write down the corresponding letter in the bottom alphabet
[keep final display]


## Atbash Cipher
1. 

	- The Atbash Cipher was originally used to encrypt the Hebrew alphabet.
[display generic related image]
2. 

	- Choose a message you want to encipher (eg. "SECRET MESSAGE")
	- Write out the whole alphabet
[animate alphabet being drawn, display "SECRET MESSAGE"]
3. 

	- Count how far along each letter is from the start of the alphabet
	- Count the same amount from the end of the alphabet, and write down the letter you get to
[animate each letter being searched for]
4. 

	- Encryption complete!
	- To get back to the original message, simply follow the exact same process.
[move encrypted message to top, re-display searching animation]
5. 

	- Summary
	- Find the position of each letter in the alphabet
	- Count the same amount from the other end to find the new letter.
[keep final display]


## Pigpen
1. 

	- The pigpen cipher is said to have originated with the Hebrew rabbis.
	- It was also used by the Freemasons, a secret society in the 18th Century
[show pigpen alphabet]
2. 

	- Choose a message you want to encipher (eg. "PIGPEN MESSAGE")
[display message at top]
3. 

	- Get a copy of the pigpen alphabet
	- The pigpen alphabet can be created by drawing a hash shape or tic-tac-toe board, then drawing a cross.
	- These 2 shapes are then repeated, placing a dot in each of the 13 sections
	- The alphabet is then written, with one letter in each of the 26 sections
[animate the shapes being drawn, then the alphabet being filled in]
4. 

	- To encipher the message, find each letter of the message in the pigpen alphabet.
	- Draw the surrounding lines and dots in order to create the new message.
[show letters being found, then drawn]
5.  
	- Encryption complete!
	- To get back to the original message, find each symbol in the pigpen alphabet, and write down it's letter.
[hide original message. Show shapes being found, and letters being written]
6.  
	- Summary
	- Write out the pigpen alphabet (using # and x)
	- Fill in the alphabet into the shapes
	- Replace each letter in the message with the shape surrounding that letter in the pigpen alphabet.
[keep final display]


## Vigenère Cipher
1. 

	- The vigenère cipher was first described by Giovan Battista Bellaso in 1553, and was named "the indecipherable cipher" until 1863
2. 

	- Choose a message you want to encipher (eg. "VIGENERE CIPHER")
	- Choose a keyword you want to encipher your message with (eg. "KEYWORD")
	- Repeat the keyword until it is the same length as the message. 
[display message with keyword underneath]
3. 

	- Create a grid of the alphabet, as shown below:
	- ![](Tutorial%20designs/vigenere.gif)
[show grid of letters only]
4. 

	- For each pair of letters (message and keyword)
	- Find the message letter in the top row of the grid
	- Find the keyword letter in the left column of the grid
	- Follow the letters down and across, then write down the letter where they intersect to create the encrypted message.
[show lines moving down and across. Highlight where they intersect]
5. 

	+ Encryption complete!
	+ To get back to the original message..
	+ Pair the enciphered message with the keyword
	+ Find each letter of the keyword in the left column of the grid
	+ Follow along the row until you find the letter in the encrypted message, then follow the column up until you reach the top, then write down the letter you reached.
6. 

	- Summary
	- Choose a keyword, and repeat it so that it's the same length as the message
	- Create the 26x26 grid of the alphabet
	- Write down the letters where each pair of message+keyword meet.
[keep display]

*following ciphers are incomplete*
## Polibius
- Create a 5x5 table of the alphabet  (combine i and j)
- Replace each letter of the message with the row and column numbers (H -> row 2, column 3 -> 23)
- To decipher, split the message into pairs of numbers
- Replace each pair of numbers with the letter at the corresponding row and column
1. 

	- The Polibius Cipher is a device invented by the Ancient Greeks Cleoxenus and Democleitus, and perfected by the Ancient Greek historian and scholar Polybius.
[Display image]

2. 

	- Choose a secret message (e.g "Polibius message")
	- Draw a 5x5 table, and fill it in with the alphabet
	- (Combine letters I and J to fit it in)
[display 5x5 grid being drawn, with numbered rows and columns]
3. 

	- For each letter:
	- Find the row and column numbers, and write them in pairs.
	- E.g. S -> column 3, row 4 -> 34
[Show letters moving through grid to encrypt message]

4. 

	- Encryption complete!
	- To decrypt the message:
	- Split the encrypted message into pairs of numbers
	- For each pair, write down the letter matching the column and row numbers to create the original message.
[show letters moving back to decrypt the message]

5.

```
- Summary:
- Write out a 5x5 grid of the alphabet with row and column headings numbered.
- Find each letter in the table, and write down it's column and row numbers to create the encrypted message.
- To decrypt the message: split the encrypted message into pairs of numbers, and write down the letter found at the corresponding column and rown to create the original message.
```

[keep final display]