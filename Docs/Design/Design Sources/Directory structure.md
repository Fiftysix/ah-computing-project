## Directory Structure

-   Project

    -   Tutorials

        -   Tutorial_A

            -   Media

                

            -   tutorialA.py    - Classes: [Tutorial]

        -   Tutorial_B

            -   ...

        -   ...

        

        -   tutorial.py    - Classes: [Tutorial Base]

    -   Games

        -   Game_A

            -   Media

                

            -   gameA.py    - Classes: [Game]

        -   Game_B

            -   ...

        -   ...

    -   Quizzes

        -   Quiz_A

            -   Media

            

            -   questions.xml

        -   Quiz_B

            -   ...

        -   ...

    

    -   options.conf
    -   scores.db
    -   main.py    - Classes: [GUI, Main, Options, Database, Media]
    -   game.py    - Classes: [Game/Tutorial Base]