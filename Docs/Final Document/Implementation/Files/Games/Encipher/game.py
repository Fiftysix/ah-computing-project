"""
This file will be executed with some variables, such as SimpleGameBase
"""
import extra
import string
from difflib import SequenceMatcher


def compare_strings(str_a, str_b):
    # remove unimportant characters
    base_str_a = ""
    for letter in str_a.upper():
        if letter in string.ascii_uppercase+"0123456789":
            base_str_a += letter
    base_str_b = ""
    for letter in str_b.upper():
        if letter in string.ascii_uppercase+"0123456789":
            base_str_b += letter

    # compare strings
    sm = SequenceMatcher(None, base_str_a, base_str_b)
    score = max(0, sm.ratio()*2-1)  # reduce score so that similarity < 0.5 gets score of 0

    return score


class Game(SimpleGameBase):
    def __init__(self, path):
        SimpleGameBase.__init__(self, path)

        self.setup()

        self.part = 1
        self.scores = []
        self.wheel = None
        self.title = "Caesar encryption"

        self.add_wheel()
        self.title_widget.setText("Encryption Game")
        self.do_next_part()

    def add_wheel(self):
        self.clear_graphic()
        self.wheel = extra.CaesarWheel()
        self.graphics_widget.layout().addWidget(self.wheel)

    def rotate_wheel(self, amount=0):
        self.wheel.set_rotation(amount)

    def do_next_part(self):
        if self.part == 1:
            self.add_text("You have a large amount of treasure that you want to pass on to a friend.")
        elif self.part == 2:
            self.add_text("You have hidden the treasure, and want to send the location to this friend, since he is too far away to meet in person")
        elif self.part == 3:
            self.add_text("You are worried that if someone else finds the messages you send to him, they will try to find the treasure themselves. Therefore you must protect these messages.")
        elif self.part == 4:
            self.add_text("You have previously communicated with this friend using the caesar cipher, using a rotation of the length of the message, divided by 4, and rounded down")
        elif self.part == 5:
            self.add_text("Part 1 of the message: \"I have hidden some treasure for you.\"")
            self.enable_input()
        elif self.part == 6:
            answer = self.answer_box.text()
            score = compare_strings(answer, "R qjen qrmmnw bxvn canjbdan oxa hxd.")
            self.scores += [score]
            self.disable_input()
            if score == 1:
                self.add_text("That message looks like it was encrypted well! Let's continue!")
            elif score >= 0.3:
                self.add_text("That will probably be understandable. Lets encrypt the next one.")
            else:
                self.add_text("That didn't work. Lets try that again.")
                self.enable_input()
                self.part -= 1
        elif self.part == 7:
            self.add_text("Part 2 of the message: \"It is buried 7 steps south from the furthest palm tree.\"")
            self.enable_input()
        elif self.part == 8:
            answer = self.answer_box.text()
            score = compare_strings(answer, "Vg vf ohevrq 7 fgrcf fbhgu sebz gur shegurfg cnyz gerr.")
            self.scores += [score]
            self.disable_input()
            if score == 1:
                self.add_text("That message looks like it was encrypted well! Let's continue!")
            elif score >= 0.3:
                self.add_text("That will probably be understandable. Lets encrypt the next one.")
            else:
                self.add_text("That didn't work. Lets try that again.")
                self.enable_input()
                self.part -= 1
        elif self.part == 9:
            self.add_text("Part 3 of the message: \"The palm tree can be found on the Pirate Peninsula, at the south of Pirate Island.\"")
            self.enable_input()
        elif self.part == 10:
            answer = self.answer_box.text()
            score = compare_strings(answer, "Nby jufg nlyy wuh vy ziohx ih nby Jcluny Jyhchmofu, un nby mionb iz Jcluny Cmfuhx.")
            self.scores += [score]
            self.disable_input()
            if score == 1:
                self.add_text("That message looks like it was encrypted well! Let's continue!")
            elif score >= 0.3:
                self.add_text("That will probably be understandable. Lets encrypt the next one.")
            else:
                self.add_text("That didn't work. Lets try that again.")
                self.enable_input()
                self.part -= 1
        elif self.part == 11:
            percentage = sum(self.scores) / len(self.scores)
            if percentage == 1:
                self.add_text("Amazing work! You have sent the message to your friend. He should find it in no time!")
            elif percentage >= 0.4:
                self.add_text("Good work. Your friend will probably find the treasure with that message.")
            else:
                self.add_text("You sent the message to your friend, but got a reply from him saying he was unable to decrypt the message.")
                self.enable_input()
                self.part -= 1

        else:
            # ended
            self.disable_input()
            self.end_quiz(sum(self.scores)/len(self.scores))

        self.part += 1
        if len(self.scores) > 0:
            percentage = sum(self.scores)/len(self.scores)
        else:
            percentage = 1
        self.score_display.setText(str(round(percentage*100))+"%")
        print("score:", percentage)

        if len(self.scores) > 4 and percentage < 0.3:
            self.add_text("This seems impossible. Maybe try again later.")
            self.disable_input()
            self.part = 56  # end the game
