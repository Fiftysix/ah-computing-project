"""
This file will be executed with some variables, such as SimpleGameBase
"""
import extra
import string
from difflib import SequenceMatcher


def compare_strings(str_a, str_b):
    # remove unimportant characters
    base_str_a = ""
    for letter in str_a.upper():
        if letter in string.ascii_uppercase+"0123456789":
            base_str_a += letter
    base_str_b = ""
    for letter in str_b.upper():
        if letter in string.ascii_uppercase+"0123456789":
            base_str_b += letter

    # compare strings
    sm = SequenceMatcher(None, base_str_a, base_str_b)
    score = max(0, sm.ratio()*2-1)  # reduce score so that similarity < 0.5 gets score of 0

    return score


class Game(SimpleGameBase):
    def __init__(self, path):
        SimpleGameBase.__init__(self, path)

        self.setup()

        self.part = 1
        self.scores = []
        self.wheel = None
        self.title = "Caesar decryption"

        self.add_wheel()
        self.title_widget.setText("Decryption Game")
        self.do_next_part()

    def add_wheel(self):
        self.clear_graphic()
        self.wheel = extra.CaesarWheel()
        self.graphics_widget.layout().addWidget(self.wheel)

    def rotate_wheel(self, amount=0):
        self.wheel.set_rotation(amount)

    def do_next_part(self):
        if self.part == 1:
            self.add_text("You are a messenger employed by a very rich person.")
        elif self.part == 2:
            self.add_text("Recently he has begun sending a large number of messages to one of his correspondents who you know to be evil.")
        elif self.part == 3:
            self.add_text("Your challenge is to decrypt the messages being sent to uncover their evil plan, and stop it before it is too late.")
        elif self.part == 4:
            self.add_text("You have a clue: they seem to use the current date to encrypt the messages")
        elif self.part == 5:
            self.add_text("4/4/89: New message received: \"Exxegomrk jvsq iewx ex qmhrmklx, ribx jypp qssr\"")  # Attacking from east at midnight, next full moon
            self.enable_input()
        elif self.part == 6:
            answer = self.answer_box.text()
            score = compare_strings(answer, "Attacking from east at midnight, next full moon")
            self.scores += [score]
            self.disable_input()
            if score == 1:
                self.add_text("You seem to have decrypted the message well. This will be useful!")
            elif score >= 0.3:
                self.add_text("The message seems to be understandable. Lets continue.")
            else:
                self.add_text("That seems wrong... Lets try that again.")
                self.enable_input()
                self.part -= 1
        elif self.part == 7:
            self.add_text("7/4/89: New message received: \"Tlla hu ovby ilmvyl ha aol avw vm zwvvrf opss\"")  # Meet an hour before at the top of spooky hill
            self.enable_input()
        elif self.part == 8:
            answer = self.answer_box.text()
            score = compare_strings(answer, "Meet an hour before at the top of spooky hill")
            self.scores += [score]
            self.disable_input()
            if score == 1:
                self.add_text("You seem to have decrypted the message well. This will be useful!")
            elif score >= 0.3:
                self.add_text("The message seems to be understandable. Lets continue.")
            else:
                self.add_text("That seems wrong... Lets try that again.")
                self.enable_input()
                self.part -= 1
        elif self.part == 9:
            self.add_text("9/4/89: New message received: \"R fruu karwp 5 vnw. Fn ljw vjtn cqrb zdrlt\"")  # I will bring 5 men. We can make this quick
            self.enable_input()
        elif self.part == 10:
            answer = self.answer_box.text()
            score = compare_strings(answer, "I will bring 5 men. We can make this quick")
            self.scores += [score]
            self.disable_input()
            if score == 1:
                self.add_text("You seem to have decrypted the message well. This will be useful!")
            elif score >= 0.3:
                self.add_text("The message seems to be understandable. Lets continue.")
            else:
                self.add_text("That seems wrong... Lets try that again.")
                self.enable_input()
                self.part -= 1
        elif self.part == 11:
            percentage = sum(self.scores) / len(self.scores)
            if percentage == 1:
                self.add_text("Amazing work! you handed in all the information to the police, and they managed to catch the thieves easily!")
            elif percentage >= 0.4:
                self.add_text("Good work. you handed in the information, and there was enough to catch the thieves just in time.")
            else:
                self.add_text("You gave your information to the police, but they were not able to understand it. The thieves got away with it.")
                self.enable_input()
                self.part -= 1

        else:
            # ended
            self.disable_input()
            self.end_quiz(sum(self.scores)/len(self.scores))

        self.part += 1
        if len(self.scores) > 0:
            percentage = sum(self.scores)/len(self.scores)
        else:
            percentage = 1
        self.score_display.setText(str(round(percentage*100))+"%")
        print("score:", percentage)

        if len(self.scores) > 4 and percentage < 0.3:
            self.add_text("This seems impossible. Maybe try again later.")
            self.disable_input()
            self.part = 56  # end the game
