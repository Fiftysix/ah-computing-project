from PyQt5 import QtWidgets, uic, QtGui, QtPrintSupport
from PyQt5.QtCore import Qt
import os
import functools
from xml.etree import ElementTree as xml
import random
import string
import extra
import database
from difflib import SequenceMatcher


def list_games(quiz_dir, game_dir):
    quizzes = []
    for dir_name in os.listdir(quiz_dir):
        path = os.path.join(quiz_dir, dir_name)
        if os.path.isdir(path) and os.path.exists(os.path.join(path, "info.conf")):
            quiz = {"Title": "<Quiz>", "Description": "<...>", "path": path}
            with open(os.path.join(path, "info.conf"), "r", encoding="utf-8") as info_file:
                for line in info_file.read().split("\n"):
                    if "=" in line:
                        name, value = line.split("=", 1)
                        quiz[name] = value
            quiz["thumbnail"] = QtGui.QPixmap(os.path.join(path, "thumbnail.png"))
            quizzes += [quiz]
    games = []
    for dir_name in os.listdir(game_dir):
        path = os.path.join(game_dir, dir_name)
        if os.path.isdir(path) and os.path.exists(os.path.join(path, "info.conf")):
            game = {"Title": "<Game>", "Description": "<...>", "path": path}
            with open(os.path.join(path, "info.conf"), "r", encoding="utf-8") as info_file:
                for line in info_file.read().split("\n"):
                    if "=" in line:
                        name, value = line.split("=", 1)
                        game[name] = value
            game["thumbnail"] = QtGui.QPixmap(os.path.join(path, "thumbnail.png"))
            games += [game]
    return quizzes + games


def compare_strings(str_a, str_b):
    # remove unimportant characters
    base_str_a = ""
    for letter in str_a.upper():
        if letter in string.ascii_uppercase+"0123456789":
            base_str_a += letter
    base_str_b = ""
    for letter in str_b.upper():
        if letter in string.ascii_uppercase+"0123456789":
            base_str_b += letter

    # compare strings
    sm = SequenceMatcher(None, base_str_a, base_str_b)
    score = max(0, sm.ratio()*2-1)  # reduce score so that similarity < 0.5 gets score of 0

    return score


class QuizUI(QtWidgets.QWidget):
    def __init__(self, path, start_page, end_page):
        QtWidgets.QWidget.__init__(self)
        uic.loadUi('Interfaces/quiz.ui', self)  # Load the .ui file
        self.number_label = self.findChild(QtWidgets.QLabel, "number")
        self.score_label = self.findChild(QtWidgets.QLabel, "score")
        self.question_label = self.findChild(QtWidgets.QLabel, "question")
        self.image_widget = extra.Image(QtGui.QPixmap())
        self.answer_area = self.findChild(QtWidgets.QFrame, "answers")
        self.back_button = self.findChild(QtWidgets.QPushButton, "back")
        self.next_button = self.findChild(QtWidgets.QPushButton, "next")

        self.path = path
        self.start_page = start_page
        self.end_page = end_page
        self.media = extra.Media(os.path.join(self.path, "media"))
        self.xml = xml.parse(os.path.join(path, "quiz.xml")).getroot()
        self.questions = self.xml.findall("question")

        self.current_question = -1
        self.current_answers = []
        self.scores = [0]*len(self.questions)  # number of question answered correctly

        self.setup()

    def setup(self):
        self.next_button.clicked.connect(self.next_page)
        self.back_button.clicked.connect(self.back_page)
        self.display_question(self.current_question)
        self.layout().addWidget(self.image_widget, 1, 0, 1, 2)

    def display_question(self, idx):
        idx = max(min(len(self.questions)-1, idx), 0)
        if idx == self.current_question:
            return
        self.current_question = idx
        self.clear_answer_widget()
        self.number_label.setText("Question "+str(self.current_question+1)+" of "+str(len(self.questions)))
        self.next_button.setText("Check")

        question = self.questions[self.current_question]

        self.question_label.setText(question.find("text").text)
        if "image" in question.attrib:
            pixmap = self.media.images[question.attrib["image"]]
            self.image_widget.setPixmap(pixmap)
            self.image_widget.show()
        else:
            self.image_widget.hide()

        answers = [answer.text for answer in question.findall("answer")]
        wrong_answers = [answer.text for answer in question.findall("wrong")]

        layout = self.answer_area.layout()
        if len(wrong_answers) == 0:
            answer_box = QtWidgets.QPlainTextEdit()
            layout.addWidget(answer_box)
        else:
            all_answers = answers+wrong_answers
            random.shuffle(all_answers)
            for answer in all_answers:
                ans = QtWidgets.QRadioButton(answer)
                layout.addWidget(ans)

        self.current_answers = answers

    def clear_answer_widget(self):
        layout = self.answer_area.layout()
        for i in range(layout.count()):
            layout.itemAt(0).widget().deleteLater()
            layout.itemAt(0).widget().setParent(None)

    def next_page(self):
        if self.next_button.text() == "Check":
            self.next_button.setText("Next")
            layout = self.answer_area.layout()
            for i in range(layout.count()):
                widget = layout.itemAt(i).widget()
                if type(widget) == QtWidgets.QRadioButton:
                    if widget.text() in self.current_answers:
                        if widget.isChecked():
                            self.scores[self.current_question] = 1
                        widget.setStyleSheet("background-color:#afa;")
                    else:
                        widget.setStyleSheet("background-color:#faa;")

                elif type(widget) == QtWidgets.QPlainTextEdit:
                    text = widget.toPlainText()
                    max_score = 0
                    for answer in self.current_answers:
                        score = compare_strings(text, answer)
                        if score > max_score:
                            max_score = score

                        answer_display = QtWidgets.QWidget()
                        answer_layout = QtWidgets.QHBoxLayout()
                        answer_display.setLayout(answer_layout)
                        left = QtWidgets.QLabel(answer)
                        right = QtWidgets.QLabel(f"{round(score*100)}%")
                        right.setAlignment(Qt.AlignRight)
                        answer_layout.addWidget(left)
                        answer_layout.addWidget(right)
                        if score > 0:
                            answer_display.setStyleSheet("background-color:#afa;")
                        else:
                            answer_display.setStyleSheet("background-color:#faa;")
                        layout.addWidget(answer_display)
                    self.scores[self.current_question] = max_score
                widget.setEnabled(False)
            percentage = sum(self.scores[:self.current_question+1])/(self.current_question+1)
            print("Quiz Score:", sum(self.scores), f"{percentage}%")
            self.score_label.setText(f"Score: {round(percentage*100)}%")
        else:
            if self.current_question == len(self.questions)-1:
                self.end_page(sum(self.scores)/len(self.scores))
            else:
                self.display_question(self.current_question+1)

    def back_page(self):
        if self.current_question > 0:
            self.display_question(self.current_question-1)
        else:
            self.start_page()


class QuizHome(QtWidgets.QWidget):
    def __init__(self, path, title):
        QtWidgets.QWidget.__init__(self)
        self.path = path
        self.title = title
        self.score = 0
        self.db = database.Database()

        layout = QtWidgets.QGridLayout()
        self.setLayout(layout)
        self.end_screen_type_select = None
        self.end_screen_name_select = None

        self.input_class_select = None
        self.input_type_select = None
        self.input_name_select = None
        self.input_name_label = None
        self.input_class_score = None

        self.display_buttons()

    def display_buttons(self):
        self.clear_layout()

        b1 = QtWidgets.QPushButton("Start Quiz")
        b1.clicked.connect(self.start_quiz)
        self.layout().addWidget(b1, 0, 0)

        b2 = QtWidgets.QPushButton("Print Quiz")
        b2.clicked.connect(self.print_quiz)
        self.layout().addWidget(b2, 0, 1)

        b3 = QtWidgets.QPushButton("Input Scores")
        b3.clicked.connect(self.input_scores)
        self.layout().addWidget(b3, 0, 2)

    def start_quiz(self):
        self.clear_layout()
        quiz = QuizUI(self.path, start_page=self.display_buttons, end_page=self.end_quiz)
        self.layout().addWidget(quiz)

    def end_quiz(self, score):
        print("ENDED", score)
        self.clear_layout()
        self.score = score

        layout = self.layout()

        label = QtWidgets.QLabel(f"Final Score:\n{round(score*100)}%")
        label.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        layout.addWidget(label, 0, 0, 1, 2)

        l1 = QtWidgets.QLabel("Save For Type:")
        l1.setAlignment(Qt.AlignRight)
        layout.addWidget(l1, 1, 0, 1, 1)
        self.end_screen_type_select = QtWidgets.QComboBox()
        self.end_screen_type_select.addItems(["Class", "Group", "Student"])
        self.end_screen_type_select.currentTextChanged.connect(self.end_screen_type_changed)
        layout.addWidget(self.end_screen_type_select, 1, 1, 1, 1)

        l2 = QtWidgets.QLabel("Save For Name:")
        l2.setAlignment(Qt.AlignRight)
        layout.addWidget(l2, 2, 0, 1, 1)
        self.end_screen_name_select = QtWidgets.QComboBox()
        self.end_screen_type_changed("Class")
        layout.addWidget(self.end_screen_name_select, 2, 1, 1, 1)

        submit = QtWidgets.QPushButton("Submit")
        submit.clicked.connect(self.end_screen_submit)
        layout.addWidget(submit, 3, 0, 1, 2)

    def end_screen_type_changed(self, text):
        self.end_screen_name_select.clear()
        if text == "Class":
            for class_ in self.db.execute("SELECT name FROM Class"):
                self.end_screen_name_select.addItem(class_.field(0).value())
        elif text == "Group":
            for group in self.db.execute("SELECT name FROM Player WHERE isGroup = 1"):
                self.end_screen_name_select.addItem(group.field(0).value())
        else:
            for group in self.db.execute("SELECT name FROM Player WHERE isGroup = 0"):
                self.end_screen_name_select.addItem(group.field(0).value())

    def end_screen_submit(self):
        type_ = self.end_screen_type_select.currentText()
        name = self.end_screen_name_select.currentText()

        if type_ == "Student":
            self.db.add_player_score(self.score, name, self.title, False)
        elif type_ == "Group":
            self.db.add_player_score(self.score, name, self.title, True)
        else:
            self.db.add_class_score(self.score, name, self.title)

        self.display_buttons()

    def print_quiz(self):
        html = "<h1>"+self.title+"<h1><hr>"

        data = xml.parse(os.path.join(self.path, "quiz.xml")).getroot()
        questions = data.findall("question")
        for question_idx in range(len(questions)):
            question = questions[question_idx]
            question_html = f"<h2>{question_idx+1}. {question.find('text').text}</h2>"
            if "image" in question.attrib:
                img_path = os.path.join(self.path, "media", question.attrib["image"])
                question_html += f"<br><img width=\"600\" src=\"{img_path}\">"

            answers = [answer.text for answer in question.findall("answer")]
            wrong_answers = [answer.text for answer in question.findall("wrong")]
            if len(wrong_answers) > 0:
                answers += wrong_answers
                random.shuffle(answers)
                for answer in answers:
                    question_html += f"<h3>□  {answer}</h3>"
            else:
                question_html += "<br>"*3

            html += question_html+"<hr>"

        document = QtGui.QTextDocument()
        document.setHtml(html)

        printer = QtPrintSupport.QPrinter()

        dialog = QtPrintSupport.QPrintDialog(printer, self)

        if dialog.exec() == QtWidgets.QDialog.Accepted:
            document.print(printer)

    def input_scores(self):
        self.clear_layout()

        layout = self.layout()

        l1 = QtWidgets.QLabel("Class:")
        l1.setAlignment(Qt.AlignRight)
        layout.addWidget(l1, 0, 0, 1, 1)
        self.input_class_select = QtWidgets.QComboBox()

        for class_ in self.db.execute("SELECT name FROM Class"):
            self.input_class_select.addItem(class_.field(0).value())

        self.input_class_select.currentTextChanged.connect(self.input_changed)
        layout.addWidget(self.input_class_select, 0, 1, 1, 2)

        l1 = QtWidgets.QLabel("Score Type:")
        l1.setAlignment(Qt.AlignRight)
        layout.addWidget(l1, 1, 0, 1, 1)
        self.input_type_select = QtWidgets.QComboBox()
        self.input_type_select.addItems(["Class", "Group", "Student"])
        self.input_type_select.setToolTip("Changes will be lost when changing types. Remember to click submit first!")
        self.input_type_select.currentTextChanged.connect(self.input_changed)
        layout.addWidget(self.input_type_select, 1, 1, 1, 2)

        self.input_name_label = QtWidgets.QLabel("Name:")
        self.input_name_label.setAlignment(Qt.AlignRight)
        layout.addWidget(self.input_name_label, 2, 0, 1, 1)
        self.input_name_select = QtWidgets.QTableWidget()
        self.input_name_select.setColumnCount(2)
        layout.addWidget(self.input_name_select, 2, 1, 1, 2)

        self.input_class_score = QtWidgets.QLabel("")
        self.input_class_score.setAlignment(Qt.AlignTop)
        layout.addWidget(self.input_class_score, 2, 1, 1, 2)

        set_score = QtWidgets.QPushButton("Set Score")
        set_score.clicked.connect(self.input_set_score)
        layout.addWidget(set_score, 3, 1, 1, 1)
        remove_score = QtWidgets.QPushButton("Remove Score")
        remove_score.clicked.connect(self.input_remove_score)
        layout.addWidget(remove_score, 3, 2, 1, 1)

        submit = QtWidgets.QPushButton("Submit")
        submit.clicked.connect(self.input_scores_submit)
        layout.addWidget(submit, 4, 0, 1, 3)

        self.input_changed()

    def input_changed(self, _=None):
        type_ = self.input_type_select.currentText()
        class_ = self.input_class_select.currentText()

        self.input_name_select.clear()
        if type_ == "Class":
            self.input_name_select.hide()
            self.input_class_score.show()
            self.input_name_label.setText("Score:")
        elif type_ == "Group":
            self.input_class_score.hide()
            self.input_name_select.show()
            self.input_name_label.setText("Name:")
            row = 0
            for group in self.db.execute(f"""SELECT name FROM Player WHERE isGroup = 1 AND className = "{class_}";"""):
                self.input_name_select.setRowCount(row+1)
                self.input_name_select.setCellWidget(row, 0, QtWidgets.QLabel(group.field(0).value()))
                self.input_name_select.setCellWidget(row, 1, QtWidgets.QLabel(""))
                row += 1
        else:  # type_ == "Student"
            self.input_class_score.hide()
            self.input_name_select.show()
            self.input_name_label.setText("Name:")
            row = 0
            for student in self.db.execute(f"""SELECT name FROM Player WHERE isGroup = 0 AND className = "{class_}";"""):
                self.input_name_select.setRowCount(row+1)
                self.input_name_select.setCellWidget(row, 0, QtWidgets.QLabel(student.field(0).value()))
                self.input_name_select.setCellWidget(row, 1, QtWidgets.QLabel(""))
                row += 1

    def input_set_score(self):
        type_ = self.input_type_select.currentText()

        score, accepted = QtWidgets.QInputDialog().getText(self, "Input Score", "Enter a score between 0 and 100")
        if accepted:
            try:  # validate input
                score = int(score.replace("%", "").strip())
                if score < 0 or score > 100:
                    raise ValueError()
            except ValueError:
                QtWidgets.QMessageBox().warning(self, "Error", "Score must be a whole number between 0 and 100.")
                return

            if type_ == "Class":
                self.input_class_score.setText(str(score)+"%")
            else:
                name_item = self.input_name_select.cellWidget(self.input_name_select.currentRow(), 0)
                if name_item is None:
                    QtWidgets.QMessageBox().warning(self, "Error", "No player selected.")
                    return

                self.input_name_select.cellWidget(self.input_name_select.currentRow(), 1).setText(str(score)+"%")

    def input_remove_score(self):
        type_ = self.input_type_select.currentText()

        if type_ == "Class":
            self.input_class_score.setText("")
        else:
            name_item = self.input_name_select.cellWidget(self.input_name_select.currentRow(), 0)
            if name_item is None:
                QtWidgets.QMessageBox().warning(self, "Error", "No player selected.")
                return

            self.input_name_select.cellWidget(self.input_name_select.currentRow(), 1).setText("")

    def input_scores_submit(self):
        type_ = self.input_type_select.currentText()
        class_ = self.input_class_select.currentText()

        if type_ == "Class":
            score_text = self.input_class_score.text()
            if score_text != "":
                score = int(score_text[:-1])
                self.db.add_class_score(score, class_, self.title)
        else:
            for row in range(self.input_name_select.rowCount()):
                name_item = self.input_name_select.cellWidget(row, 0)
                name = name_item.text()
                score_text = self.input_name_select.cellWidget(row, 1).text()
                if score_text != "":
                    score = int(score_text[:-1])
                    self.db.add_player_score(score, name, self.title, type_ == "Group")

    def clear_layout(self):
        layout = self.layout()
        for i in range(layout.count()):
            layout.itemAt(0).widget().deleteLater()
            layout.itemAt(0).widget().setParent(None)


class SimpleGameBase(QtWidgets.QWidget):
    def __init__(self, path):
        QtWidgets.QWidget.__init__(self)
        uic.loadUi('Interfaces/simple_game.ui', self)  # Load the .ui file

        self.title_widget = self.findChild(QtWidgets.QLabel, "Title")
        self.answer_box = self.findChild(QtWidgets.QLineEdit, "answer_input")
        self.answer_submit = self.findChild(QtWidgets.QPushButton, "answer_submit")
        self.continue_button = self.findChild(QtWidgets.QPushButton, "continue_button")
        self.graphics_widget = self.findChild(QtWidgets.QWidget, "graphic_display")
        self.text_display = self.findChild(QtWidgets.QTextBrowser, "text_display")
        self.rotate_value = self.findChild(QtWidgets.QSpinBox, "rotate_value")
        self.rotate_submit = self.findChild(QtWidgets.QPushButton, "rotate_submit")
        self.score_display = self.findChild(QtWidgets.QLabel, "score")

        self.path = path
        self.media = extra.Media(os.path.join(self.path, "media"))
        self.text = []

        self.db = None
        self.end_screen_type_select = None
        self.end_screen_name_select = None
        self.score = 0

    def setup(self):
        graphics_layout = QtWidgets.QVBoxLayout()
        self.graphics_widget.setLayout(graphics_layout)
        self.answer_submit.clicked.connect(self.do_next_part)
        self.rotate_submit.clicked.connect(self.rotate)
        self.continue_button.clicked.connect(self.do_next_part)

    def rotate(self):
        rotation = self.rotate_value.value()
        self.rotate_wheel(rotation)

    def add_text(self, text):
        self.text = [text] + self.text
        text = "<center>"+"<br><br>".join(self.text)+"</center>"
        self.text_display.document().setHtml(text)

    def enable_input(self):
        self.answer_box.setEnabled(True)
        self.answer_submit.setEnabled(True)
        self.continue_button.setEnabled(False)

    def disable_input(self):
        self.answer_box.setEnabled(False)
        self.answer_submit.setEnabled(False)
        self.answer_box.setText("")
        self.continue_button.setEnabled(True)

    def clear_graphic(self):
        print("..a")
        layout = self.graphics_widget.layout()
        for i in range(layout.count()):
            print("..b")
            layout.itemAt(0).widget().deleteLater()
            layout.itemAt(0).widget().setParent(None)

    def next_page(self):
        if self.page < self.num_pages:
            self.display_page(self.page+1)

    def back_page(self):
        if self.page > 1:
            self.display_page(self.page-1)

    def clear_layout(self):
        layout = self.layout()
        for i in range(layout.count()):
            layout.itemAt(0).widget().deleteLater()
            layout.itemAt(0).widget().setParent(None)

    def end_quiz(self, score):
        self.db = database.Database()
        print("ENDED", score)
        self.clear_layout()
        self.score = score

        layout = self.layout()

        label = QtWidgets.QLabel(f"Final Score:\n{round(score*100)}%")
        label.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        layout.addWidget(label, 0, 0, 1, 2)

        l1 = QtWidgets.QLabel("Save For Type:")
        l1.setAlignment(Qt.AlignRight)
        layout.addWidget(l1, 1, 0, 1, 1)
        self.end_screen_type_select = QtWidgets.QComboBox()
        self.end_screen_type_select.addItems(["Class", "Group", "Student"])
        self.end_screen_type_select.currentTextChanged.connect(self.end_screen_type_changed)
        layout.addWidget(self.end_screen_type_select, 1, 1, 1, 1)

        l2 = QtWidgets.QLabel("Save For Name:")
        l2.setAlignment(Qt.AlignRight)
        layout.addWidget(l2, 2, 0, 1, 1)
        self.end_screen_name_select = QtWidgets.QComboBox()
        self.end_screen_type_changed("Class")
        layout.addWidget(self.end_screen_name_select, 2, 1, 1, 1)

        submit = QtWidgets.QPushButton("Submit")
        submit.clicked.connect(self.end_screen_submit)
        layout.addWidget(submit, 3, 0, 1, 2)

    def end_screen_type_changed(self, text):
        self.end_screen_name_select.clear()
        if text == "Class":
            for class_ in self.db.execute("SELECT name FROM Class"):
                self.end_screen_name_select.addItem(class_.field(0).value())
        elif text == "Group":
            for group in self.db.execute("SELECT name FROM Player WHERE isGroup = 1"):
                self.end_screen_name_select.addItem(group.field(0).value())
        else:
            for group in self.db.execute("SELECT name FROM Player WHERE isGroup = 0"):
                self.end_screen_name_select.addItem(group.field(0).value())

    def end_screen_submit(self):
        type_ = self.end_screen_type_select.currentText()
        name = self.end_screen_name_select.currentText()

        if type_ == "Student":
            self.db.add_player_score(self.score, name, self.title, False)
        elif type_ == "Group":
            self.db.add_player_score(self.score, name, self.title, True)
        else:
            self.db.add_class_score(self.score, name, self.title)
        self.clear_layout()


class GameHandler(QtWidgets.QWidget):
    def __init__(self, quiz_path, game_path):
        QtWidgets.QWidget.__init__(self)
        self.games = list_games(quiz_path, game_path)
        self.sub_widget = None
        self.setup()

        self.playing = False  # if a game is in progress
        self.game = None

    def setup(self):
        layout = QtWidgets.QVBoxLayout()
        self.sub_widget = QtWidgets.QWidget()
        layout.addWidget(self.sub_widget)
        self.setLayout(layout)

    def set_main_widget(self, widget):
        self.layout().removeWidget(self.sub_widget)
        self.sub_widget.deleteLater()
        self.sub_widget = widget
        self.layout().addWidget(self.sub_widget)

    def display_buttons(self):
        layout = QtWidgets.QGridLayout()

        row = 0
        column = 0
        for game in self.games:
            button = QtWidgets.QPushButton()
            button.clicked.connect(functools.partial(self.load_game, game["Title"]))
            button.setObjectName("gameButton")
            button.setSizePolicy(
                QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding))
            button_layout = QtWidgets.QVBoxLayout()
            button.setLayout(button_layout)
            image = extra.Image(game["thumbnail"])
            image.setObjectName("gameThumbnail")
            title = QtWidgets.QLabel(game["Title"])
            desc = QtWidgets.QLabel(game["Description"])
            button_layout.addWidget(image, 1)
            button_layout.addWidget(title)
            button_layout.addWidget(desc)
            layout.addWidget(button, row, column)

            column += 1
            if column == 3:
                row += 1
                column = 0

        new_widget = QtWidgets.QWidget()
        new_widget.setLayout(layout)
        self.set_main_widget(new_widget)

    def load_game(self, title):
        print("Started Game:", title)
        game = None
        for g in self.games:
            if g["Title"] == title:
                game = g
                break

        if os.path.exists(os.path.join(game["path"], "quiz.xml")):
            self.game = QuizHome(game["path"], game["Title"])
            self.set_main_widget(self.game)
        else:
            code_path = os.path.join(game["path"], "game.py")
            code_file = open(code_path)
            program = compile(code_file.read(), code_path, "exec")
            variables = {"SimpleGameBase": SimpleGameBase, "Handler": self, "__name__": "Game"}

            exec(program, variables)  # "variables" will be updated with variables resulting from file execution
            self.game = variables["Game"](game["path"])
            self.set_main_widget(self.game)

        self.playing = True
