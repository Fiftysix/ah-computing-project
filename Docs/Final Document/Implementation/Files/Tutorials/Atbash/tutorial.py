"""
This file will be executed with globals:
    "TutorialBase": TutorialBase
    "Handler": <Tutorials.handler.TutorialHandler object at 0x0...>
    "__name__": "Tutorial"
"""
import extra
from PyQt5 import QtCore
import string


class Tutorial(TutorialBase):
    def __init__(self, message, path):
        TutorialBase.__init__(self, path)
        self.num_pages = 5
        self.grid = None
        self.message = None
        self.encrypted = None

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.send_letter)
        self.encrypted_message = ""
        self.flying_timer_idx = 0
        self.flying_letters = []

        self.setup()

    def graphic_1(self, graphic_level):
        self.clear_graphic()
        image = self.media.image_widget("cipher.png")
        self.graphics_widget.layout().addWidget(image)

    def graphic_2(self, graphic_level):
        self.clear_graphic()
        self.remove_flying_letters()

        self.message = extra.ValueLabel("Message", "SECRET MESSAGE")
        self.graphics_widget.layout().addWidget(self.message, 0, 0)

        self.grid = extra.LetterGrid([list(string.ascii_uppercase)]*2)
        self.graphics_widget.layout().addWidget(self.grid, 1, 0)
        self.grid.animate_display(lambda: self.grid.flip_rows([2]))

        self.encrypted = extra.ValueLabel("Encrypted Message", "")
        self.graphics_widget.layout().addWidget(self.encrypted, 2, 0)

    def graphic_3(self, graphic_level):
        if graphic_level != 2:
            self.graphic_2(graphic_level)
        self.grid.end_animation()

        self.encrypted_message = ""

        self.flying_timer_idx = 0
        self.send_letter()

        self.timer.setInterval(6000)
        self.timer.start()

    def send_letter(self):
        start = self.message.get_position(self.flying_timer_idx)

        letter = self.message.value[self.flying_timer_idx]
        if letter not in string.ascii_uppercase:
            end = self.encrypted.get_next_position(self.encrypted_message, letter)
            self.encrypted_message += letter
            path = [start, end]
        else:
            mid = self.grid.get_position(start["value"], 0, [1])
            encrypted_letter = mid[1]["value"]
            end = self.encrypted.get_next_position(self.encrypted_message, encrypted_letter)
            self.encrypted_message += encrypted_letter
            path = [start] + mid + [end]
        self.flying_letters += [extra.FlyingLetter(path, self.graphics_widget, lambda l: self.encrypted.add_value(l))]

        self.flying_timer_idx += 1
        if self.flying_timer_idx >= len(self.message.value):
            self.timer.stop()
        elif self.flying_timer_idx > 1:
            self.timer.setInterval(1000)  # letters after the first one have a shorter delay

    def remove_flying_letters(self):
        if len(self.flying_letters) > 0:
            for letter in self.flying_letters:
                letter.stop_animation()
            for child in self.graphics_widget.children():
                if type(child) == extra.FlyingLetter:
                    child.setParent(None)
                    child.deleteLater()

            self.timer.stop()
            self.flying_letters = []

            self.encrypted.value = "HVXIVG NVHHZTV"
            self.encrypted.add_value("")
            self.message.value = "SECRET MESSAGE"
            self.message.add_value("")

    def graphic_4(self, graphic_level):
        if graphic_level != 3:
            self.graphic_3(graphic_level)
        self.remove_flying_letters()

        self.message.value = self.encrypted.value
        self.message.name = "Encrypted Message"
        self.encrypted.value = ""
        self.encrypted.name = "Message"

        self.message.add_value("")
        self.encrypted.add_value("")

        self.encrypted_message = ""

        self.flying_timer_idx = 0
        self.send_letter()

        self.timer.setInterval(6000)
        self.timer.start()

    def graphic_5(self, graphic_level):
        if graphic_level != 4:
            self.graphic_4(graphic_level)
        self.remove_flying_letters()

        self.encrypted.name = "Encrypted Message"
        self.encrypted.value = "HVXIVG NVHHZTV"
        self.message.name = "Message"
        self.message.value = "SECRET MESSAGE"

        self.message.add_value("")
        self.encrypted.add_value("")
