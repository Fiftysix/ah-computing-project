"""
This file will be executed with variables:
    "TutorialBase": <Tutorials.handler.TutorialBass class>
    "Handler": <Tutorials.handler.TutorialHandler object>
    "__name__": "Tutorial"
"""
import extra
from PyQt5.QtCore import Qt
from PyQt5 import QtCore
import string


class Tutorial(TutorialBase):
    def __init__(self, message, path):
        TutorialBase.__init__(self, path)

        self.num_pages = 7
        self.setup()
        self.encrypted_message = ""
        self.flying_timer_idx = 0
        self.flying_letters = []

        self.timer = QtCore.QTimer()

        self.wheel = None
        self.message = None
        self.encrypted = None

    def graphic_1(self, graphic_level):
        self.clear_graphic()
        image = self.media.image_widget("cipher.png")
        self.graphics_widget.layout().addWidget(image)

    def graphic_2(self, graphic_level):
        self.clear_graphic()
        self.remove_flying_letters()
        self.message = extra.ValueLabel("Message", "SECRET MESSAGE")
        self.graphics_widget.layout().addWidget(self.message, 0, 0)
        key = extra.ValueLabel("Key", 6, Qt.AlignRight)
        self.graphics_widget.layout().addWidget(key, 0, 1)
        self.wheel = extra.CaesarWheel(draw_amount=-1)
        self.graphics_widget.layout().addWidget(self.wheel, 1, 0, 1, 2)

        self.encrypted = extra.ValueLabel("Encrypted Message", "", hidden=True)
        self.graphics_widget.layout().addWidget(self.encrypted, 2, 0, 1, 2)

    def graphic_3(self, graphic_level):
        if graphic_level != 2:
            self.graphic_2(graphic_level)
        self.wheel.animate_draw(6)

    def graphic_4(self, graphic_level):
        if graphic_level != 3:
            self.graphic_3(graphic_level)
        self.encrypted.add_value("")
        self.wheel.stop_animating()

        self.encrypted_message = ""

        self.flying_timer_idx = 0
        self.send_letter()

        self.timer.setInterval(6000)
        try:  # disconnect raises TypeError if there is nothing to disconnect
            self.timer.disconnect()
        except TypeError:
            pass
        self.timer.timeout.connect(self.send_letter)
        self.timer.start()

    def send_letter(self):
        start = self.message.get_position(self.flying_timer_idx)
        mid = self.wheel.get_position(start["value"])
        encrypted_letter = mid[1]["value"]

        letter = self.message.value[self.flying_timer_idx]
        if letter not in string.ascii_uppercase:
            end = self.encrypted.get_next_position(self.encrypted_message, letter)
            self.encrypted_message += letter
            path = [start, end]
        else:
            end = self.encrypted.get_next_position(self.encrypted_message, encrypted_letter)
            self.encrypted_message += encrypted_letter
            path = [start] + mid + [end]
        self.flying_letters += [extra.FlyingLetter(path, self.graphics_widget, lambda l: self.encrypted.add_value(l))]

        self.flying_timer_idx += 1
        if self.flying_timer_idx >= len(self.message.value):
            self.timer.stop()
        elif self.flying_timer_idx > 1:
            self.timer.setInterval(1000)  # letters after the first one have a shorter delay

    def remove_flying_letters(self):
        if len(self.flying_letters) > 0:
            for letter in self.flying_letters:
                letter.stop_animation()
            for child in self.graphics_widget.children():
                if type(child) == extra.FlyingLetter:
                    child.setParent(None)
                    child.deleteLater()

            self.timer.stop()
            self.flying_letters = []

            self.encrypted.value = "YKIXKZ SKYYGMK"
            self.encrypted.add_value("")
            self.message.value = "SECRET MESSAGE"
            self.message.add_value("")

    def graphic_5(self, graphic_level):
        if graphic_level not in (4, 5, 6):
            self.graphic_4(graphic_level)
        self.remove_flying_letters()

    def graphic_6(self, graphic_level):
        if graphic_level != 5:
            self.graphic_5(graphic_level)

        self.message.value = ""
        self.message.add_value("")
        self.encrypted_message = ""
        self.flying_timer_idx = 0

        self.send_back_letter()
        self.timer.setInterval(6000)
        try:  # disconnect raises TypeError if there is nothing to disconnect
            self.timer.disconnect()
        except TypeError:
            pass
        self.timer.timeout.connect(self.send_back_letter)
        self.timer.start()

    def send_back_letter(self):
        start = self.encrypted.get_position(self.flying_timer_idx)
        mid = self.wheel.get_position("SECRET MESSAGE"[self.flying_timer_idx])
        encrypted_letter = mid[0]["value"]

        letter = self.encrypted.value[self.flying_timer_idx]
        if letter not in string.ascii_uppercase:
            end = self.message.get_next_position(self.encrypted_message, letter)
            self.encrypted_message += letter
            path = [start, end]
        else:
            end = self.message.get_next_position(self.encrypted_message, encrypted_letter)
            self.encrypted_message += encrypted_letter
            path = [start] + [mid[1]] + [mid[0]] + [end]
        self.flying_letters += [extra.FlyingLetter(path, self.graphics_widget, lambda l: self.message.add_value(l))]

        self.flying_timer_idx += 1
        if self.flying_timer_idx >= len(self.encrypted.value):
            self.timer.stop()
        elif self.flying_timer_idx > 1:
            self.timer.setInterval(1000)  # letters after the first one have a shorter delay

    def graphic_7(self, graphic_level):
        if graphic_level != 6 and graphic_level != 7:
            self.graphic_6(graphic_level)
        self.remove_flying_letters()
