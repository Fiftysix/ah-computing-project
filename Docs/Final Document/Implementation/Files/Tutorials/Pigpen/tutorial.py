"""
This file will be executed with variables:
    "TutorialBase": <Tutorials.handler.TutorialBass class>
    "Handler": <Tutorials.handler.TutorialHandler object>
    "__name__": "Tutorial"
"""
import extra
from PyQt5.QtCore import Qt
from PyQt5 import QtCore, QtWidgets, QtGui
import string


left = (0, 0, 0, 1)
right = (1, 0, 1, 1)
top = (0, 0, 1, 0)
bottom = (0, 1, 1, 1)

letters = {
    "A": [
        (0, 0, 1/3, 1/3),  # translator rect
        bottom,  # first line position within translator rect
        right  # second line
    ],
    "B": [
        (1/3, 0, 1/3, 1/3),
        left, bottom, right
    ],
    "C": [
        (2/3, 0, 1/3, 1/3),
        left, bottom
    ],
    "D": [
        (0, 1/3, 1/3, 1/3),
        top, right, bottom
    ],
    "E": [
        (1/3, 1/3, 1/3, 1/3),
        top, right, bottom, left
    ],
    "F": [
        (2/3, 1/3, 1/3, 1/3),
        top, left, bottom
    ],
    "G": [
        (0, 2/3, 1/3, 1/3),
        top, right
    ],
    "H": [
        (1/3, 2/3, 1/3, 1/3),
        left, top, right
    ],
    "I": [
        (2/3, 2/3, 1/3, 1/3),
        left, top
    ],
    
    # second area:
    "J": [
        (0, 0, 1/3, 1/3),
        bottom, right,
        (5/6, 5/6)  # dot
    ],
    "K": [
        (1/3, 0, 1/3, 1/3),
        left, bottom, right,
        (3/6, 5/6)
    ],
    "L": [
        (2/3, 0, 1/3, 1/3),
        left, bottom,
        (1/6, 5/6)
    ],
    "M": [
        (0, 1/3, 1/3, 1/3),
        top, right, bottom,
        (5/6, 3/6)
    ],
    "N": [
        (1/3, 1/3, 1/3, 1/3),
        top, right, bottom, left,
        (3/6, 3/6)
    ],
    "O": [
        (2/3, 1/3, 1/3, 1/3),
        top, left, bottom,
        (1/6, 3/6)
    ],
    "P": [
        (0, 2/3, 1/3, 1/3),
        top, right,
        (5/6, 1/6)
    ],
    "Q": [
        (1/3, 2/3, 1/3, 1/3),
        left, top, right,
        (3/6, 1/6)
    ],
    "R": [
        (2/3, 2/3, 1/3, 1/3),
        left, top,
        (1/6, 1/6)
    ],

    # third area:
    "S": [
        (0, 0, 1, 1/2),
        (0, 0, 1/2, 1),
        (1/2, 1, 1, 0)
    ],
    "T": [
        (0, 0, 1/2, 1),
        (0, 0, 1, 1/2),
        (1, 1/2, 0, 1)
    ],
    "U": [
        (1/2, 0, 1/2, 1),
        (1, 0, 0, 1/2),
        (0, 1/2, 1, 1)
    ],
    "V": [
        (0, 1/2, 1, 1/2),
        (0, 1, 1/2, 0),
        (1/2, 0, 1, 1)
    ],

    # fourth area:
    "W": [
        (0, 0, 1, 1/2),
        (0, 0, 1/2, 1),
        (1/2, 1, 1, 0),
        (1/2, 2/3)
    ],
    "X": [
        (0, 0, 1/2, 1),
        (0, 0, 1, 1/2),
        (1, 1/2, 0, 1),
        (2/3, 1/2)
    ],
    "Y": [
        (1/2, 0, 1/2, 1),
        (1, 0, 0, 1/2),
        (0, 1/2, 1, 1),
        (1/3, 1/2)
    ],
    "Z": [
        (0, 1/2, 1, 1/2),
        (0, 1, 1/2, 0),
        (1/2, 0, 1, 1),
        (1/2, 1/3)
    ],
}


def draw_pig(painter, x, y, w, h, letter, morph=0):
    if letter in letters:
        data = letters[letter]
        area = data[0]
        area = (area[0]*morph*w+x, area[1]*morph*h+y, (1-(1-area[2])*morph)*w, (1-(1-area[3])*morph)*h)
        items = data[1:]
        for item in items:
            if len(item) == 4:
                x1 = item[0]*area[2]+area[0]
                y1 = item[1]*area[3]+area[1]
                x2 = item[2]*area[2]+area[0]
                y2 = item[3]*area[3]+area[1]
                painter.drawLine(x1, y1, x2, y2)
            elif len(item) == 2:
                x_ = (0.5-(0.5-item[0])*morph)*area[2]+area[0]
                y_ = (0.5-(0.5-item[1])*morph)*area[3]+area[1]
                painter.drawEllipse(x_-2, y_-2, 4, 4)
    else:
        font = QtGui.QFont()
        font.setPointSize(min(w, h)*0.5)
        painter.setFont(font)
        painter.drawText(x, y, w, h, Qt.AlignVCenter | Qt.AlignHCenter, letter)


class FlyingPig(QtWidgets.QWidget):
    def __init__(self, canvas, letter, start, end, on_finished=lambda: None, reverse=False):
        QtWidgets.QWidget.__init__(self, parent=canvas)
        self.letter = letter
        self.morph = 1
        self.canvas = canvas
        self.on_finished = on_finished

        self.raise_()

        duration = 1000

        self.rect_animation = QtCore.QPropertyAnimation(self, b"geometry")
        self.rect_animation.setDuration(duration)
        self.rect_animation.setStartValue(start)
        self.rect_animation.setEndValue(end)

        self.morph_animation = QtCore.QPropertyAnimation(self, b"_morph")
        self.morph_animation.setDuration(duration)
        if reverse:
            self.morph_animation.setStartValue(0)
            self.morph_animation.setEndValue(1)
        else:
            self.morph_animation.setStartValue(1)
            self.morph_animation.setEndValue(0)

        self.rect_animation.finished.connect(self.on_anim_finished)

        self.rect_animation.start()
        self.morph_animation.start()

        self.show()

    @QtCore.pyqtProperty(float)
    def _morph(self):
        return self.morph

    @_morph.setter
    def _morph(self, value):
        self.morph = value
        # self.repaint()

    def paintEvent(self, q_paint_event):
        painter = QtGui.QPainter(self)
        painter.setPen(QtGui.QPen(Qt.black, 4, Qt.SolidLine))
        draw_pig(painter, 0, 0, self.width(), self.height(), self.letter, self.morph)

    def on_anim_finished(self):
        self.setParent(None)
        self.deleteLater()
        self.on_finished()

    def stop_animation(self):
        self.rect_animation.stop()
        self.morph_animation.stop()


class PigPen(QtWidgets.QWidget):
    def __init__(self, hidden=False):
        QtWidgets.QWidget.__init__(self)
        self.hidden = hidden
        self.font_size = 15
        self.encrypted_font_size = 15

        self.draw_amount = 51
        self.draw_amount_animation = QtCore.QPropertyAnimation(self, b"_draw_amount")
        self.draw_amount_animation.setStartValue(0)
        self.draw_amount_animation.setEndValue(51)
        self.draw_amount_animation.setDuration(12000)

        self.message = ""
        self.message_write_distance = 0
        self.flying_timer_idx = 0
        self.flying_pigs = []

    @QtCore.pyqtProperty(int)
    def _draw_amount(self):
        return self.draw_amount

    @_draw_amount.setter
    def _draw_amount(self, value):
        self.draw_amount = value
        self.repaint()

    def paintEvent(self, q_paint_event):
        if self.hidden:
            return

        width = self.width()
        height = self.height()

        size = min(width/8, height/8.5)
        x = (width-size*7)/2
        y = (height-size*7)/2-size/2

        painter = QtGui.QPainter(self)

        font = QtGui.QFont()
        self.font_size = size*0.5
        font.setPointSize(self.font_size)
        painter.setFont(font)
        painter.setPen(QtGui.QPen(Qt.black, 4, Qt.SolidLine))

        draw_amount = self.draw_amount

        draw_amount = self.tic_tac(painter, x, y, size, False, 0, draw_amount)
        draw_amount = self.tic_tac(painter, x+size*4, y, size, True, 9, draw_amount)
        draw_amount = self.cross(painter, x, y+size*4, size, False, 18, draw_amount)
        self.cross(painter, x+size*4, y+size*4, size, True, 22, draw_amount)

        self.encrypted_font_size = max(width/35, 15)
        font.setPointSize(self.encrypted_font_size)
        painter.setFont(font)
        painter.drawText(QtCore.QRect(0, height-size, width-size, size), Qt.AlignVCenter, "Encrypted Message: ")

        metrics = QtGui.QFontMetrics(font)
        text_area = metrics.boundingRect("Encrypted Message: ")
        left = text_area.width()
        width = text_area.height()/2
        for letter_idx in range(len(self.message)):
            if letter_idx < self.message_write_distance:
                letter = self.message[letter_idx]
                left += width*1.4
                draw_pig(painter, left, height-size*0.75, width, width, letter)

    def tic_tac(self, painter, x, y, size, dotted, start_idx, draw_amount):
        lines = [
            (x + size, y, x + size, y + size * 3),
            (x + size * 2, y, x + size * 2, y + size * 3),
            (x, y + size, x + size * 3, y + size),
            (x, y + size * 2, x + size * 3, y + size * 2)
        ]
        for line in lines:
            if draw_amount > 0:
                painter.drawLine(*line)
            draw_amount -= 1

        row = 0
        col = 0
        for char_idx in range(start_idx, start_idx+9):
            char = string.ascii_uppercase[char_idx]

            if draw_amount > 0:
                painter.drawText(QtCore.QRect(x+col*size, y+row*size, size, size), Qt.AlignVCenter | Qt.AlignHCenter, char)
            draw_amount -= 1
            if dotted:
                left = x+(size*(5/6))+(size*(4/6))*col - 2
                top = y+(size*(5/6))+(size*(4/6))*row - 2
                if draw_amount > 8:
                    painter.drawEllipse(left, top, 4, 4)

            col += 1
            if col == 3:
                col = 0
                row += 1
        if dotted:
            draw_amount -= 9
        return draw_amount

    def cross(self, painter, x, y, size, dotted, start_idx, draw_amount):
        if draw_amount > 0:
            painter.drawLine(x, y, x+size*3, y+size*3)
        if draw_amount > 1:
            painter.drawLine(x+size*3, y, x, y+size*3)
        draw_amount -= 2

        text_areas = [
            (x, y, size*3, size),
            (x, y, size, size*3),
            (x+size*2, y, size, size*3),
            (x, y+size*2, size*3, size)
        ]
        for i in range(len(text_areas)):
            if draw_amount > 0:
                painter.drawText(QtCore.QRect(*text_areas[i]), Qt.AlignHCenter | Qt.AlignVCenter, string.ascii_uppercase[start_idx+i])
            draw_amount -= 1

        if dotted:
            dots = [
                (x+size*1.5, y+size),
                (x+size, y+size*1.5),
                (x+size*2, y+size*1.5),
                (x+size*1.5, y+size*2)
            ]
            for dot in dots:
                if draw_amount > 0:
                    painter.drawEllipse(*dot, 4, 4)
                draw_amount -= 1

        return draw_amount

    def animate_draw(self):
        self.hidden = False
        self.draw_amount_animation.start()

    def stop_animating(self):
        self.draw_amount_animation.stop()
        self.draw_amount = 51
        self.remove_flying_pigs()

        self.repaint()

    def send_pig(self, reverse=False, on_end=lambda: None):
        width = self.width()
        height = self.height()

        size = min(width/8, height/8.5)
        x = (width-size*7)/2
        y = (height-size*7)/2-size/2

        letter = self.message[self.flying_timer_idx]
        letter_idx = string.ascii_uppercase.find(letter)
        if letter_idx > 21:
            left = x+size*4
            top = y+size*4
        elif letter_idx > 17:
            left = x
            top = y+size*4
        elif letter_idx > 8:
            left = x+size*4
            top = y
        else:
            left = x
            top = y
        start = QtCore.QRect(left, top, size*3, size*3)

        font = QtGui.QFont()
        font.setPointSize(self.encrypted_font_size)

        metrics = QtGui.QFontMetrics(font)
        text_area = metrics.boundingRect("Encrypted Message: ")
        width = text_area.height() / 2
        left = text_area.width() + width*1.4*(self.flying_timer_idx+1)
        top = height - size * 0.75
        end = QtCore.QRect(left, top, width, width)

        if reverse:
            FlyingPig(self, letter, end, start, lambda: self._show_next_letter(on_end), True)
        else:
            FlyingPig(self, letter, start, end, self._show_next_letter)

        self.flying_timer_idx += 1

    def remove_flying_pigs(self):
        if len(self.flying_pigs) > 0:
            for pig in self.flying_pigs:
                pig.stop_animation()
            for child in self.graphics_widget.children():
                if type(child) == FlyingPig:
                    child.setParent(None)
                    child.deleteLater()

            self.timer.stop()
            self.flying_pigs = []

    def get_position(self, letter):
        width = self.width()
        height = self.height()

        size = min(width/8, height/8.5)
        x = (width-size*7)/2
        y = (height-size*7)/2-size/2
        letter_idx = string.ascii_uppercase.find(letter)
        if letter_idx > 21:
            left = x + size * 4
            top = y + size * 4
        elif letter_idx > 17:
            left = x
            top = y + size * 4
        elif letter_idx > 8:
            left = x + size * 4
            top = y
        else:
            left = x
            top = y
        if letter not in letters:
            return {"rect": QtCore.QRect(left, top, size, size), "value": letter, "size": self.font_size}
        data = letters[letter]
        area = data[0]
        w = h = size*3
        area = (area[0]*w+left+self.x(), area[1]*h+top+self.y(), (1-(1-area[2]))*w, (1-(1-area[3]))*h)
        return {"rect": QtCore.QRect(*area), "value": letter, "size": self.font_size}

    def _show_next_letter(self, on_end=lambda: None):
        self.message_write_distance += 1
        on_end()
        self.repaint()


class Tutorial(TutorialBase):
    def __init__(self, message, path):
        TutorialBase.__init__(self, path)

        self.num_pages = 6
        self.setup()
        self.encrypted_message = ""
        self.flying_timer_idx = 0
        self.flying_letters = []
        self.message_text = message
        self.decrypting = False

        self.timer = QtCore.QTimer()
        self.timer.setInterval(1000)
        self.timer.timeout.connect(self.send_letter)
        self.flying_timer_idx = 0
        self.flying_letters = []

        self.pigpen = None
        self.message = None

    def graphic_1(self, graphic_level):
        self.clear_graphic()
        image = self.media.image_widget("cipher.png")
        self.graphics_widget.layout().addWidget(image)

    def graphic_2(self, graphic_level):
        self.remove_flying_letters()
        self.clear_graphic()
        self.message = extra.ValueLabel("Message", "SECRET MESSAGE")
        self.graphics_widget.layout().addWidget(self.message, 0, 0)

        self.pigpen = PigPen(hidden=True)
        self.graphics_widget.layout().addWidget(self.pigpen, 1, 0)

    def graphic_3(self, graphic_level):
        if graphic_level != 2:
            self.graphic_2(graphic_level)

        self.pigpen.animate_draw()

    def graphic_4(self, graphic_level):
        if graphic_level != 3:
            self.graphic_3(graphic_level)
        self.pigpen.stop_animating()

        self.pigpen.message = self.message_text
        self.flying_timer_idx = 0
        self.pigpen.flying_timer_idx = 0
        self.decrypting = False

        self.send_letter()
        self.timer.start()

    def send_letter(self, part_2=False):
        if self.decrypting and not part_2:
            self.pigpen.send_pig(True, lambda: self.send_letter(True))
            if self.pigpen.flying_timer_idx == len(self.message_text):
                self.timer.stop()
            return
        start = self.message.get_position(self.flying_timer_idx, self.message_text)
        end = self.pigpen.get_position(self.message_text[self.flying_timer_idx])
        path = [start, end]
        on_end = lambda l: self.pigpen.send_pig()
        if self.decrypting:
            path = [end, start]
            on_end = lambda l: self.message.add_value(l)
        self.flying_letters += [extra.FlyingLetter(path, self.graphics_widget, on_end, 2000)]

        self.flying_timer_idx += 1
        if self.flying_timer_idx == len(self.message_text):
            self.timer.stop()

    def remove_flying_letters(self):
        if len(self.flying_letters) > 0:
            for letter in self.flying_letters:
                letter.on_finished = lambda l: None
                letter.stop_animation()
            for child in self.graphics_widget.children():
                if type(child) == extra.FlyingLetter:
                    child.setParent(None)
                    child.deleteLater()

            self.timer.stop()
            self.flying_letters = []

    def graphic_5(self, graphic_level):
        if graphic_level != 4:
            self.graphic_4(graphic_level)
        self.pigpen.stop_animating()
        self.pigpen.message_write_distance = len(self.message_text)
        self.remove_flying_letters()

        self.flying_timer_idx = 0
        self.pigpen.flying_timer_idx = 0
        self.decrypting = True
        self.message.value = ""
        self.message.add_value("")

        self.send_letter()
        self.timer.start()

    def graphic_6(self, graphic_level):
        if graphic_level != 5:
            self.graphic_5(graphic_level)
        self.pigpen.stop_animating()
        self.remove_flying_letters()
        self.message.value = self.message_text
        self.message.add_value("")
