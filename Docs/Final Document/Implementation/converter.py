import os
from pygments import highlight
from pygments.lexers import get_lexer_for_filename, configs
from pygments.formatters import HtmlFormatter
lines = 0


def convert_file(path, document):
    global lines
    extension = os.path.splitext(path)[1]
    file_path = "Files"+path
    print(file_path)
    document += f"<br><p class=\"filepath\">{path}</p>"
    if extension == ".png":
        document += f"<img src=\"{file_path}\">"
    elif extension == ".conf":
        file_ = open(file_path, "rb").read()
        lines += len(file_.split(b"\n"))
        document += highlight(file_, configs.KconfigLexer(), HtmlFormatter()).replace("\n", "\n<code></code>").replace("<pre>", "<pre><code></code>")
    else:
        file_ = open(file_path, "rb").read()
        lines += len(file_.split(b"\n"))
        document += highlight(file_, get_lexer_for_filename(path), HtmlFormatter()).replace("\n", "\n<code></code>").replace("<pre>", "<pre><code></code>")

    return document


def convert_dir(path, document=""):
    for item in os.listdir("Files/"+path):
        if os.path.isfile("Files/"+path+"/"+item):
            document = convert_file(path+"/"+item, document)
        else:
            document = convert_dir(path+"/"+item, document)
    return document


if __name__ == "__main__":
    content = convert_dir("")
    output = f"""
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
{content}
</body>
</html>
    """
    open("output.html", "w").write(output)
    print(lines)
