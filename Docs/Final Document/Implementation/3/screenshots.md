# Screenshots
>   Start screen, displaying 3 options

![image-20200420172457121](image-20200420172457121.png)

>   Tutorial selection screen

![image-20200420195635438](image-20200420195635438.png)

>   Game/Quiz selection screen

![image-20200510191355825](image-20200510191355825.png)

>   Theme Options page

![image-20200420172648799](image-20200420172648799.png)

>   Themed interface

![image-20200420181146053](image-20200420181146053.png)

>   Database search page

![image-20200510191516317](image-20200510191516317.png)

> Database search results

![image-20200510191714789](image-20200510191714789.png)



>   Database management page

![image-20200420172757157](image-20200420172757157.png)

>   About menu

![image-20200420195932115](image-20200420195932115.png)



>   Hidden sidebar

![image-20200420172932001](image-20200420172932001.png)

>   Polibius cipher tutorial

![image-20200420173244962](image-20200420173244962.png)

>   Caesar cipher tutorial

![image-20200420173324678](image-20200420173324678.png)

>   Pigpen cipher tutorial

![image-20200420173446222](image-20200420173446222.png)

>   Route cipher tutorial

![image-20200420173509223](image-20200420173509223.png)

>   Vigenère cipher tutorial

![image-20200420173542564](image-20200420173542564.png)

>   Atbash cipher tutorial

![image-20200420173617045](image-20200420173617045.png)

>   Tutorial opening page example

![image-20200420173713043](image-20200420173713043.png)

>   tutorial mid-animation example

![image-20200420173742464](image-20200420173742464.png)

>   Tutorial animated letter movement example

![image-20200420173816855](image-20200420173816855.png)

>   Tutorial animated letters reversing example

![image-20200420173852277](image-20200420173852277.png)

>   Quiz start page

![image-20200420180547770](image-20200420180547770.png)

>   Multiple choice quiz question

![image-20200420174045082](image-20200420174045082.png)

>   Multiple choice quiz question answers

![image-20200420174117274](image-20200420174117274.png)

>   Decryption question, with partially incorrect answer

![image-20200420174308330](image-20200420174308330.png)

>   Decryption question

![image-20200420174358999](image-20200420174358999.png)

>   Quiz end screen, showing score submission

![image-20200420174508363](image-20200420174508363.png)

>   Quiz print dialog (shows native OS printer dialog)

![image-20200420174607469](image-20200420174607469.png)



>   Score input, for entering scores of multiple players

![image-20200420174716159](image-20200420174716159.png)

>   Decryption Game

![image-20200510192011406](image-20200510192011406.png)

>   Encryption Game

![image-20200510192128129](image-20200510192128129.png)



## The following pages contain an example of a printed quiz

