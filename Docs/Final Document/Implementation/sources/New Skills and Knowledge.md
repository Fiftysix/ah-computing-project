# New Skills and Knowledge

During the development of this project I have had to improve and increase my knowledge of different ***things***. I have tried to describe some of what I have learned below.

## Python and Qt / PyQt

The code for my project uses python standard libraries and the PyQt python wrapper for Qt.

Although I was familiar with the basics of Qt GUI development, I learned a lot more than what I knew at the beginning, such as:

**QtSql** - a library giving easy access to connecting to multiple database formats, and allowing you to execute SQL. I learned how to connect to a SQLite database, how to execute SQL in that database, and how to handle results from queries using python.

**QPrintSupport** - a library for printing documents. I learned how to create documents, and print it via the system printer dialog.

**Animations** - I learned how to animate parameters of qt widgets, which required researching PyQt specific functions in order to access the Qt functionality, how to move widgets between different positions on the screen and drawing shapes and images onto a canvas (QPaintDevice).

**xml.etree** - I learned how to use this library to parse XML files, including searching for types of nodes, and reading node parameters.

**difflib.SequenceMatcher** - I learned to use the SequenceMatcher from the difflib module in order to find the similarity between objects, which I used to validate the user's answers.

## Git

I learned to use basic git to commands to manage my project. I learned to use an ssh URL to push my changes to gitlab (where I hosted my project) using an ED25519 key pair. I also learned the basic git commands: push, pull, commit, add and status.

## Ciphers

Since the program teaches ciphers, I had to learn how they worked myself. I learned how to encrypt and decrypt using 6 different ciphers.

## Planning

I learned how to plan a project and all it's components in more detail than I learned during the AH course, because I was experiencing the process myself. I learned how it didn't always go to plan, and how designs and ideas changed as I worked on the project.

## Interviewing

At the beginning of the project I had to interview a teacher who was acting as a client. These were my first times interviewing someone, and I had to learn to prepare enough questions to make the interview worth it, as well as write down answers in a brief but useful way.