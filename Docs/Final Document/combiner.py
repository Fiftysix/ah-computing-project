import os


names = []
for name in open("files.txt", "r").read().split("\n"):
    if len(name) > 0:
        names += ['"'+name+'"']

command = "pdfunite "+" ".join(names)+' "compiled.pdf"'
print(command)
os.system(command)
