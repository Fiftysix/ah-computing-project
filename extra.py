from PyQt5 import QtGui, QtWidgets, QtCore
from PyQt5.QtCore import Qt
import os
import string
import math


class Image(QtWidgets.QLabel):
    def __init__(self, pixmap):
        QtWidgets.QLabel.__init__(self)
        self.setObjectName("image")
        if type(pixmap) == str:
            pixmap = QtGui.QPixmap(pixmap)
        self.pixmap = pixmap

    def paintEvent(self, event):
        size = self.size()
        painter = QtGui.QPainter(self)
        scaledPix = self.pixmap.scaled(self.size(), Qt.KeepAspectRatio, Qt.FastTransformation)
        imgSize = scaledPix.size()
        x = (size.width()-imgSize.width())/2
        y = (size.height()-imgSize.height())/2
        painter.drawPixmap(x, y, scaledPix)

    def setPixmap(self, pixmap):
        self.pixmap = pixmap
        self.repaint()


# class to load and store all images in a folder
class Media:
    def __init__(self, path=None):
        self.images = {}
        if path is not None and os.path.exists(path):
            self.load_media(path)

    def load_media(self, path):
        for name in os.listdir(path):
            if name != "sources.txt":
                self.images[name] = QtGui.QPixmap(os.path.join(path, name))

        return self.images

    def image_widget(self, name):
        image = Image(self.images[name])
        return image


def get_font(base_widget):
    # intended to get a font object matching the UI's font
    base_widget.style().unpolish(base_widget)
    base_widget.style().polish(base_widget)
    base_widget.update()
    # above code seems to have no effect
    return base_widget.font()


class GUI(QtWidgets.QWidget):
    def __init__(self, *args, **kwargs):
        QtWidgets.QWidget.__init__(self, *args, **kwargs)


class CaesarWheel(QtWidgets.QWidget):
    def __init__(self, animate=False, rotation=0, draw_amount=52):
        QtWidgets.QWidget.__init__(self)
        self.rotation = rotation
        self.future_rotation = rotation
        self.rot_anim = QtCore.QPropertyAnimation(self, b"_rotation")
        self.draw_amount = draw_amount
        self.draw_anim = QtCore.QPropertyAnimation(self, b"_draw_amount")

        if animate:
            self.rotation = 0
            self.animate_draw(self.future_rotation)

    # create a property to be used by QPropertyAnimation
    @QtCore.pyqtProperty(float)
    def _rotation(self):
        return self.rotation

    @_rotation.setter
    def _rotation(self, value):
        self.rotation = value
        self.repaint()

    @QtCore.pyqtProperty(int)
    def _draw_amount(self):
        return self.draw_amount

    @_draw_amount.setter
    def _draw_amount(self, value):
        self.draw_amount = value
        self.repaint()

    def paintEvent(self, q_paint_event):
        if self.draw_amount == -1:
            return
        width = self.width()
        height = self.height()
        square = min(width, height)
        space = square/10
        double = 2*space

        painter = QtGui.QPainter(self)
        painter.setPen(QtGui.QPen(Qt.black, 4, Qt.SolidLine))
        painter.drawEllipse(space, space, square-double, square-double)
        painter.drawEllipse(space*2, space*2, square-2*double, square-2*double)
        painter.drawEllipse(space*3, space*3, square-3*double, square-3*double)

        font = get_font(self)
        font.setPointSize(space/2)
        painter.setFont(font)

        for pos in range(26):
            if self.draw_amount < pos:
                break
            letter = string.ascii_uppercase[pos]
            x = y = square/2
            angle = (2*math.pi)*((pos+self.rotation)/26)
            x += ((square-1.5*double)/2)*math.sin(angle)
            y -= ((square-1.5*double)/2)*math.cos(angle)
            painter.drawText(x-space/2, y-space/2, space, space, Qt.AlignVCenter | Qt.AlignHCenter, letter)

        for pos in range(26):
            if self.draw_amount-26 < pos:
                break
            letter = string.ascii_uppercase[pos]
            x = y = square/2
            angle = (2*math.pi)*(pos/26)
            x += ((square-2.5*double)/2)*math.sin(angle)
            y -= ((square-2.5*double)/2)*math.cos(angle)
            painter.drawText(x-space/2, y-space/2, space, space, Qt.AlignVCenter | Qt.AlignHCenter, letter)

    def animate_draw(self, rotation=0, delay=100):
        self.future_rotation = rotation
        self.draw_anim.setDuration(delay*52)
        self.draw_anim.setStartValue(0)
        self.draw_anim.setEndValue(52)
        self.draw_anim.finished.connect(lambda: self.set_rotation(self.future_rotation))
        self.draw_anim.start()

    def set_rotation(self, rotation, delay=350):
        distance = abs(self.rotation-rotation)
        self.rot_anim.setDuration(delay*distance)
        self.rot_anim.setStartValue(self.rotation)
        self.rot_anim.setEndValue(rotation)
        self.rot_anim.start()

    def stop_animating(self):
        self.draw_anim.stop()
        self.rot_anim.stop()
        self.draw_amount = 52
        self.rotation = self.future_rotation
        self.repaint()

    def get_position(self, letter):
        pos = round(string.ascii_uppercase.find(letter)+self.rotation) % 26

        square = min(self.width(), self.height())
        space = square/10
        double = 2*space
        angle = (2 * math.pi) * (pos / 26)

        points = [
            {
                "value": string.ascii_uppercase[round(pos-self.rotation)],
                "rect": QtCore.QRect((square-space)/2 + ((square-1.5*double)/2) * math.sin(angle) + self.x(),
                                    (square-space)/2 - ((square-1.5*double)/2) * math.cos(angle) + self.y(),
                                    space, space),
                "size": double/4
            },
            {
                "value": string.ascii_uppercase[round(pos)],
                "rect": QtCore.QRect((square-space)/2 + ((square-2.5*double)/2) * math.sin(angle) + self.x(),
                                     (square-space)/2 - ((square-2.5*double)/2) * math.cos(angle) + self.y(),
                                     space, space),
                "size": double/4
            }]
        return points


class LetterGrid(QtWidgets.QWidget):
    def __init__(self, rows, hidden=False, header=False):
        QtWidgets.QWidget.__init__(self)
        self.rows = rows
        self.font_size = 15
        self.hidden = hidden
        self.header = header

        self.animate_flip_scale = 0  # from 0 to 1
        self.animate_flip_rows = []
        self.row_flip_animation = QtCore.QPropertyAnimation(self, b"_flip_scale")

        self.row_flip_animation.setDuration(3000)
        self.row_flip_animation.setStartValue(0)
        self.row_flip_animation.setEndValue(1)
        self.row_flip_animation.finished.connect(self.row_flip_ended)
        self.row_flip_animating = False

        self.animate_draw_amount = len(rows)*len(rows[0])
        self.on_draw_amount_end = lambda: 0
        self.draw_amount_animation = QtCore.QPropertyAnimation(self, b"_draw_amount")
        self.draw_amount_animating = False

        self.draw_amount_animation.setDuration(5000)
        self.draw_amount_animation.setStartValue(0)
        self.draw_amount_animation.setEndValue(self.animate_draw_amount)
        self.draw_amount_animation.finished.connect(self.draw_amount_ended)

        self.path = []
        self.path_distance = 0
        self.path_animation = QtCore.QPropertyAnimation(self, b"_path_distance")

        self.path_animation.setDuration(5000)
        self.path_animation.setStartValue(0)

    @QtCore.pyqtProperty(float)
    def _flip_scale(self):
        return self.animate_flip_scale

    @_flip_scale.setter
    def _flip_scale(self, value):
        self.animate_flip_scale = value
        self.repaint()

    @QtCore.pyqtProperty(int)
    def _draw_amount(self):
        return self.animate_draw_amount

    @_draw_amount.setter
    def _draw_amount(self, value):
        self.animate_draw_amount = value
        self.repaint()

    @QtCore.pyqtProperty(int)
    def _path_distance(self):
        return self.path_distance

    @_path_distance.setter
    def _path_distance(self, value):
        self.path_distance = value
        self.repaint()

    def paintEvent(self, q_paint_event):
        if self.hidden:
            return
        width = self.width()
        height = self.height()

        rows = len(self.rows)
        cols = len(self.rows[0])
        size = min(width/(cols+1), height/(rows+1))

        h = size*rows
        w = size*cols
        x = width/2-w/2
        y = height/2-h/2

        painter = QtGui.QPainter(self)
        font = get_font(self)
        self.font_size = size*0.7
        font.setPointSize(self.font_size)
        painter.setFont(font)

        painter.setPen(QtGui.QPen(Qt.black, 2, Qt.SolidLine))
        painter.drawRect(x, y, w, h)

        for col_idx in range(cols):
            if col_idx > self.header:
                painter.setPen(QtGui.QPen(Qt.black, 0.5, Qt.SolidLine))
            painter.drawLine(x + col_idx * size, y, x + col_idx * size, y + h)
        painter.setPen(QtGui.QPen(Qt.black, 2, Qt.SolidLine))
        for row_idx in range(rows):
            if row_idx > self.header:
                painter.setPen(QtGui.QPen(Qt.black, 0.5, Qt.SolidLine))
            painter.drawLine(x, y+row_idx*size, x+w, y+row_idx*size)
            for col_idx in range(cols):
                letter_x = x+col_idx*size
                if row_idx in self.animate_flip_rows:
                    letter_x = (letter_x-width/2+size/2)*(self.animate_flip_scale*-2+1)+width/2-size/2
                if self.animate_draw_amount > row_idx*cols+col_idx:
                    painter.drawText(letter_x, y+row_idx*size, size, size, Qt.AlignHCenter | Qt.AlignVCenter, self.rows[row_idx][col_idx])

        painter.setPen(QtGui.QPen(Qt.black, 3, Qt.DashLine))
        for point_idx in range(len(self.path)-1):
            if point_idx < self.path_distance:
                x1 = self.path[point_idx][0]*size+x+size/2
                y1 = self.path[point_idx][1]*size+y+size/2
                x2 = self.path[point_idx+1][0]*size+x+size/2
                y2 = self.path[point_idx+1][1]*size+y+size/2
                painter.drawLine(x1, y1, x2, y2)

    def flip_rows(self, rows):
        self.animate_flip_rows = [row-1 for row in rows]
        self.row_flip_animating = True
        self.row_flip_animation.start()

    def row_flip_ended(self):
        self.row_flip_animating = False
        for row_idx in self.animate_flip_rows:
            self.rows[row_idx] = self.rows[row_idx][::-1]
        self.animate_flip_rows = []
        self.animate_flip_scale = 0
        self.repaint()

    def draw_amount_ended(self):
        self.draw_amount_animating = False
        self.on_draw_amount_end()

    def animate_display(self, on_end=lambda: 0):
        self.hidden = False
        self.on_draw_amount_end = on_end
        self.draw_amount_animating = True
        self.draw_amount_animation.start()

    def draw_path(self, path):
        self.path = path
        self.path_distance = 0
        self.path_animation.setEndValue(len(self.path))
        self.path_animation.start()

    def end_animation(self):
        if self.draw_amount_animating:
            self.draw_amount_animation.stop()
            self.animate_draw_amount = len(self.rows)*len(self.rows[0])
            self.on_draw_amount_end()

        if self.row_flip_animating:
            self.row_flip_animation.stop()
            self.row_flip_ended()

        self.path_animation.stop()
        self.path_distance = len(self.path)
        self.repaint()

    def get_position(self, letter, row_start, row_steps):
        width = self.width()
        height = self.height()

        rows = len(self.rows)
        cols = len(self.rows[0])
        size = min(width/(cols+1), height/(rows+1))

        h = size * rows
        w = size * cols
        x = width / 2 - w / 2
        y = height / 2 - h / 2

        index = self.rows[row_start].index(letter)
        if self.header and index == 0:
            index = 1

        steps = [{
            "rect": QtCore.QRect(x+size*index+self.x(), y+size*row_start+self.y(), size, size),
            "value": letter,
            "size": self.font_size
        }]
        for row_idx in row_steps:
            steps += [{
                "rect": QtCore.QRect(x+size*index+self.x(), y+size*row_idx+self.y(), size, size),
                "value": self.rows[row_idx][index],
                "size": self.font_size
            }]
        return steps

    def convert_positions(self, positions):
        width = self.width()
        height = self.height()

        rows = len(self.rows)
        cols = len(self.rows[0])
        size = min(width / (cols + 1), height / (rows + 1))

        x = (width - size*cols)/2
        y = (height - size*rows)/2

        steps = []
        for pos in positions:
            steps += [{
                "rect": QtCore.QRect(x+size*pos[0]+self.x(), y+size*pos[1]+self.y(), size, size),
                "value": self.rows[pos[1]][pos[0]],
                "size": self.font_size
            }]
        return steps


class ValueLabel(QtWidgets.QLabel):
    def __init__(self, name, value, alignment=Qt.AlignLeft, hidden=False):
        if hidden:
            QtWidgets.QLabel.__init__(self, "")
        else:
            QtWidgets.QLabel.__init__(self, name+": "+repr(value))

        self.value = value
        self.name = name
        self.font_size = 13

        self.setAlignment(alignment | Qt.AlignTop)

    def resizeEvent(self, event):
        width = self.parentWidget().width()
        self.font_size = max(width/45, 15)
        self.setMaximumHeight(self.font_size*2)
        self.setStyleSheet(f"font-size: {self.font_size}pt;")
        QtWidgets.QLabel.resizeEvent(self, event)

    def add_value(self, value):
        self.value += value
        self.setText(self.name+": "+repr(self.value))

    def get_position(self, index, value=None):
        value = value if value is not None else self.value
        return self.get_next_position(value[:index], value[index])

    def get_next_position(self, current, new):
        # finds the Rect character 'new' would have if it was added to the end of the current value, assuming current value is 'current'
        text = self.name+": "+repr(current)
        if text[-1] == "'":
            text = text[:-1]  # remove trailing quote resulting from repr function
        font = get_font(self)
        font.setPointSize(self.font_size)
        metrics = QtGui.QFontMetrics(font)

        rect_without = metrics.boundingRect(text)
        rect = metrics.boundingRect(new)

        result = QtCore.QRect(rect_without.right()+self.x(), self.y(), rect.width(), rect.height())

        return {"rect": result, "value": new, "size": self.font_size}


# animates a letter between positions (source -> encoder -> result)
class FlyingLetter(QtWidgets.QLabel):
    def __init__(self, data, canvas, on_finished=lambda x: 0, duration=-1):
        QtWidgets.QLabel.__init__(self, parent=canvas)
        self.setAlignment(Qt.AlignVCenter | Qt.AlignHCenter)
        self.step_pos = 0
        self.data = data
        self.on_finished = on_finished
        self.canvas = canvas

        self.raise_()

        duration = duration if duration != -1 else 6000/(len(data)-1)
        self.rect_anim = QtCore.QPropertyAnimation(self, b"geometry")
        self.rect_anim.setDuration(duration)
        self.size_anim = QtCore.QPropertyAnimation(self, b"_font_size")
        self.size_anim.setDuration(duration)

        self.rect_anim.finished.connect(self.anim_finished)

        self.animate_step()
        self.show()

    @QtCore.pyqtProperty(int)
    def _font_size(self):
        return self.font().pointSize()

    @_font_size.setter
    def _font_size(self, value):
        self.setStyleSheet(f"font-size: {value}pt;text-align: center;")

    def animate_step(self):
        from_ = self.data[self.step_pos]
        to = self.data[self.step_pos+1]
        self.setText(from_["value"])

        self.rect_anim.stop()
        self.rect_anim.setStartValue(from_["rect"])
        self.rect_anim.setEndValue(to["rect"])

        self.size_anim.setStartValue(from_["size"])
        self.size_anim.setEndValue(to["size"])

        self.rect_anim.start()
        self.size_anim.start()

    def anim_finished(self):
        self.step_pos += 1
        if self.step_pos == len(self.data)-1:
            self.on_finished(self.data[-1]["value"])
            self.setParent(None)
            self.deleteLater()
        else:
            self.animate_step()

    def stop_animation(self):
        self.rect_anim.stop()
        self.size_anim.stop()
        self.on_finished(self.data[-1]["value"])
