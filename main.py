#!/usr/bin/env python
from PyQt5 import QtWidgets, uic, QtCore, Qt, QtGui
import sys
import Tutorials.handler as tutorials
import game as games
import database



# home screen
class Home(QtWidgets.QWidget):
    def __init__(self, ui):
        super(Home, self).__init__()
        uic.loadUi('Interfaces/home.ui', self)  # Load the .ui file
        self.ui = ui
        self.tutorial_button = self.findChild(QtWidgets.QPushButton, "homeTutorialButton")
        self.games_button = self.findChild(QtWidgets.QPushButton, "homeGamesButton")
        self.options_button = self.findChild(QtWidgets.QPushButton, "homeOptionsButton")
        self.setup()

    def setup(self):
        self.tutorial_button.clicked.connect(lambda: self.ui.load_view("Tutorials"))
        self.games_button.clicked.connect(lambda: self.ui.load_view("Games"))
        self.options_button.clicked.connect(lambda: self.ui.load_view("Options"))


# options screen
class Options(QtWidgets.QWidget):
    def __init__(self, ui):
        QtWidgets.QWidget.__init__(self)
        uic.loadUi('Interfaces/options.ui', self)  # Load the .ui file
        self.ui = ui
        self.db = database.Database()
        self.result_display = QtWidgets.QDialog()

        self.search_tab = self.findChild(QtWidgets.QWidget, "SearchDatabase")
        self.manage_tab = self.findChild(QtWidgets.QWidget, "ManageDatabase")
        self.theme_tab = self.findChild(QtWidgets.QWidget, "Themes")

        self.search_tab_components = {}
        self.manage_tab_components = {}
        self.theme_tab_components = {}

        self.setup()

    def setup(self):
        self.search_tab_components = {
            "searchFor": self.search_tab.findChild(QtWidgets.QComboBox, "searchFor"),
            "nameClass": self.search_tab.findChild(QtWidgets.QComboBox, "nameClass"),
            "game": self.search_tab.findChild(QtWidgets.QComboBox, "game"),
            "searchButton": self.search_tab.findChild(QtWidgets.QPushButton, "searchButton")
        }
        self.search_tab_components["searchButton"].clicked.connect(self.search_submit)
        self.search_tab_components["searchFor"].currentTextChanged.connect(self.search_for_changed)
        self.search_for_changed("Class")
        for game in self.db.execute("SELECT name FROM Game"):
            self.search_tab_components["game"].addItem(game.field(0).value())

        self.manage_tab_components = {
            "class_select": self.manage_tab.findChild(QtWidgets.QComboBox, "class_select"),
            "add_class": self.manage_tab.findChild(QtWidgets.QPushButton, "add_class"),
            "delete_class": self.manage_tab.findChild(QtWidgets.QPushButton, "delete_class"),
            "student_list": self.manage_tab.findChild(QtWidgets.QListWidget, "student_list"),
            "add_student":  self.manage_tab.findChild(QtWidgets.QPushButton, "add_student"),
            "remove_student":  self.manage_tab.findChild(QtWidgets.QPushButton, "remove_student"),

            "group_select": self.manage_tab.findChild(QtWidgets.QComboBox, "group_select"),
            "add_group": self.manage_tab.findChild(QtWidgets.QPushButton, "add_group"),
            "delete_group": self.manage_tab.findChild(QtWidgets.QPushButton, "delete_group"),
            "member_list": self.manage_tab.findChild(QtWidgets.QListWidget, "member_list"),
            "add_member":  self.manage_tab.findChild(QtWidgets.QPushButton, "add_member"),
            "remove_member":  self.manage_tab.findChild(QtWidgets.QPushButton, "remove_member"),
        }
        self.manage_tab_components["class_select"].currentTextChanged.connect(self.manage_class_select_changed)
        self.manage_tab_components["add_class"].clicked.connect(self.manage_add_class)
        self.manage_tab_components["delete_class"].clicked.connect(self.manage_delete_class)
        self.manage_tab_components["add_student"].clicked.connect(self.manage_add_student)
        self.manage_tab_components["remove_student"].clicked.connect(self.manage_remove_student)

        self.manage_tab_components["group_select"].currentTextChanged.connect(self.manage_group_select_changed)
        self.manage_tab_components["add_group"].clicked.connect(self.manage_add_group)
        self.manage_tab_components["delete_group"].clicked.connect(self.manage_delete_group)
        self.manage_tab_components["add_member"].clicked.connect(self.manage_add_member)
        self.manage_tab_components["remove_member"].clicked.connect(self.manage_remove_member)

        for class_ in self.db.execute("SELECT name FROM Class"):
            self.manage_tab_components["class_select"].addItem(class_.field(0).value())

        self.theme_tab_components = {
            "theme_name": self.theme_tab.findChild(QtWidgets.QComboBox, "themeName"),
            "theme_accept": self.theme_tab.findChild(QtWidgets.QPushButton, "themeAccept"),
        }

        self.theme_tab_components["theme_accept"].clicked.connect(self.switch_theme)

    def set_page(self, title):
        self.findChild(QtWidgets.QTabWidget, "optionTabs").setCurrentIndex(["Themes", "Search", "Manage"].index(title))

    def search_for_changed(self, text):
        if text == "Class":
            class_combobox = self.search_tab_components["nameClass"]
            class_combobox.clear()
            for class_ in self.db.execute("SELECT name FROM Class"):
                class_combobox.addItem(class_.field(0).value())

        elif text == "Group":
            group_combobox = self.search_tab_components["nameClass"]
            group_combobox.clear()
            for group in self.db.execute("SELECT name FROM Player WHERE isGroup = 1"):
                group_combobox.addItem(group.field(0).value())

        else:
            student_combobox = self.search_tab_components["nameClass"]
            student_combobox.clear()
            for group in self.db.execute("SELECT name FROM Player WHERE isGroup = 0"):
                student_combobox.addItem(group.field(0).value())

    def search_submit(self):
        search_type = self.search_tab_components["searchFor"].currentText()
        name = self.search_tab_components["nameClass"].currentText()
        game = self.search_tab_components["game"].currentText()

        class_name = name if search_type == "Class" else False
        game_name = game if game != "All" else False
        player_name = name if search_type != "Class" else False
        result = self.db.search_scores("Name", class_name, game_name, player_name)

        if search_type == "Class":
            self.result_display = database.ResultDisplay(self, result, ["Student Scores", "Group Scores", "Class Scores"])
        elif search_type == "Group":
            self.result_display = database.ResultDisplay(self, [result[1]], ["Group Scores"])
        else:  # search_type == "Student"
            self.result_display = database.ResultDisplay(self, [result[0]], ["Student Scores"])

    def manage_class_select_changed(self, text):
        self.manage_tab_components["student_list"].clear()
        for student in self.db.execute(f"SELECT name FROM Player WHERE isGroup = 0 AND className = \"{text}\""):
            QtWidgets.QListWidgetItem(student.field(0).value(), self.manage_tab_components["student_list"])
        self.manage_tab_components["group_select"].clear()
        for group in self.db.execute(f"SELECT name FROM Player WHERE isGroup = 1 AND className = \"{text}\""):
            self.manage_tab_components["group_select"].addItem(group.field(0).value())

    def manage_add_class(self):
        name, accepted = QtWidgets.QInputDialog().getText(self, "Add Class", "Class Name:")
        if accepted:
            self.db.add_class(name)
            self.manage_tab_components["class_select"].addItem(name)

    def manage_delete_class(self):
        class_select = self.manage_tab_components["class_select"]
        name = class_select.currentText()
        selection = QtWidgets.QMessageBox().question(self, "Delete Class", f'Are you sure you want to delete "{name}"?')
        if selection == QtWidgets.QMessageBox.Yes:
            self.db.delete_class(name)
            class_select.removeItem(class_select.findText(name))

    def manage_add_student(self):
        name, accepted = QtWidgets.QInputDialog().getText(self, "Add Student", "Student Name:")
        if accepted:
            class_ = self.manage_tab_components["class_select"].currentText()
            self.db.add_student(class_, name)
            QtWidgets.QListWidgetItem(name, self.manage_tab_components["student_list"])

    def manage_remove_student(self):
        student_list = self.manage_tab_components["student_list"]
        for item in student_list.selectedItems():
            name = item.text()
            selection = QtWidgets.QMessageBox().question(self, "Remove Student", f'Are you sure you want to remove "{name}"?')
            if selection == QtWidgets.QMessageBox.Yes:
                self.db.delete_player(name, False)
                student_list.takeItem(student_list.row(item))

    def manage_group_select_changed(self, text):
        self.manage_tab_components["member_list"].clear()
        members = self.db.execute(f"SELECT description FROM Player WHERE isGroup = 1 AND name = \"{text}\"")
        if len(members) == 1:
            for member in members[0].field(0).value().split(","):
                if member != "":
                    QtWidgets.QListWidgetItem(member, self.manage_tab_components["member_list"])

    def manage_add_group(self):
        name, accepted = QtWidgets.QInputDialog().getText(self, "Add Group", "Group Name:")
        if accepted:
            class_ = self.manage_tab_components["class_select"].currentText()
            self.db.add_group(class_, name)
            self.manage_tab_components["group_select"].addItem(name)

    def manage_delete_group(self):
        group_select = self.manage_tab_components["group_select"]
        name = group_select.currentText()
        selection = QtWidgets.QMessageBox().question(self, "Delete Group", f'Are you sure you want to delete "{name}"?')
        if selection == QtWidgets.QMessageBox.Yes:
            self.db.delete_player(name, True)
            group_select.removeItem(group_select.findText(name))

    def manage_add_member(self):
        name, accepted = QtWidgets.QInputDialog().getText(self, "Add Member", "Member Name:")
        if accepted:
            class_ = self.manage_tab_components["class_select"].currentText()
            group = self.manage_tab_components["group_select"].currentText()
            self.db.add_member(class_, group, name)
            QtWidgets.QListWidgetItem(name, self.manage_tab_components["member_list"])

    def manage_remove_member(self):
        member_list = self.manage_tab_components["member_list"]
        for item in member_list.selectedItems():
            name = item.text()
            selection = QtWidgets.QMessageBox().question(self, "Remove Member", f'Are you sure you want to remove "{name}"?')
            if selection == QtWidgets.QMessageBox.Yes:
                class_ = self.manage_tab_components["class_select"].currentText()
                group = self.manage_tab_components["group_select"].currentText()
                self.db.remove_member(class_, group, name)
                member_list.takeItem(member_list.row(item))

    def switch_theme(self):
        name = self.theme_tab_components["theme_name"].currentText()
        print("setting theme:", name)
        if name == "Colourful":
            self.window().setStyleSheet(open("themes/colour.css").read())
        else:
            self.window().setStyleSheet(open("themes/default.css").read())


# about dialog
class About(QtWidgets.QDialog):
    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        uic.loadUi('Interfaces/about.ui', self)  # Load the .ui file


# main window
class Window(QtWidgets.QMainWindow):
    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)  # Call the inherited classes __init__ method
        uic.loadUi('Interfaces/main.ui', self)  # Load the .ui file
        self.sidebar = self.findChild(QtWidgets.QDockWidget, "sidebar")
        self.mainArea = self.findChild(QtWidgets.QWidget, "mainArea")
        self.menuAbout = self.findChild(QtWidgets.QAction, "actionAbout")

        self.menuAbout.triggered.connect(self.about)

        self.currentMain = QtWidgets.QWidget()
        self.tree = self.sidebar.findChild(QtWidgets.QTreeView, "sidebarTree")
        self.setup_sidebar()
        self.setup_main()
        self.load_view("Start")
        self.show()  # Show the GUI

    def setup_sidebar(self):
        minimise = QtWidgets.QToolButton()
        minimise.setCheckable(True)
        minimise.clicked.connect(self.toggle_sidebar)
        minimise.setChecked(True)
        self.sidebar.setTitleBarWidget(minimise)
        self.tree.setMinimumSize(200, 0)
        self.tree.setCurrentItem(self.tree.topLevelItem(0))
        self.tree.topLevelItem(3).setExpanded(True)
        self.tree.itemClicked.connect(self.select_sidebar)

        tutorial_item = self.tree.topLevelItem(1)
        for tutorial in tutorials.list_tutorials("Tutorials"):
            tutorial_widget = QtWidgets.QTreeWidgetItem([tutorial["Title"]])
            tutorial_item.addChild(tutorial_widget)

        game_item = self.tree.topLevelItem(2)
        for game in games.list_games("Quizzes", "Games"):
            game_widget = QtWidgets.QTreeWidgetItem([game["Title"]])
            game_item.addChild(game_widget)

        self.sidebar.setWidget(self.tree)

    def setup_main(self):
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.currentMain)
        self.mainArea.setLayout(layout)

    def select_sidebar(self, current):
        title = current.text(0)
        if title in ("Games", "Tutorials"):
            current.setExpanded(True)
        child = False
        parent = current.parent()
        if parent and parent.text(0) in ("Games", "Tutorials", "Options"):
            child = title
            title = parent.text(0)
        self.load_view(title, child)

    def toggle_sidebar(self, checked):
        tree = self.sidebar.findChild(QtWidgets.QTreeView, "sidebarTree")
        if checked:
            tree.show()
        else:
            tree.hide()

    def load_view(self, title, child=False):
        print(title, child)
        self.mainArea.layout().removeWidget(self.currentMain)
        self.currentMain.deleteLater()
        self.currentMain = QtWidgets.QWidget()
        if title == "Start":
            self.currentMain = Home(self)
        elif title == "Options":
            self.currentMain = Options(self)
            if child is not False:
                self.currentMain.set_page(child)
        elif title == "Tutorials":
            self.currentMain = tutorials.TutorialHandler("Tutorials")
            if child is False:
                self.currentMain.display_buttons()
            else:
                self.currentMain.load_tutorial(child)
        elif title == "Games":
            self.currentMain = games.GameHandler("Quizzes", "Games")
            if child is False:
                self.currentMain.display_buttons()
            else:
                self.currentMain.load_game(child)
        self.mainArea.layout().addWidget(self.currentMain)

    def about(self):
        print("about")
        About().exec_()


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = Window()

    # apply themes:
    app.setStyle("Breeze")
    QtGui.QFontDatabase.addApplicationFont("themes/comic-sans-ms/COMIC.TTF")
    window.setStyleSheet(open("themes/base.css").read())

    app.exec_()  # Start the program


if __name__ == "__main__":
    main()
